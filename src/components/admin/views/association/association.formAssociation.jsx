import {postMember, putMember} from "controler/apiService/apiService.club";
import {ControllerStateUpdateClub, defaultStateClub} from "controler/control.data.club";
import React, {useEffect, useState} from "react";
import "styles/users.styles/users.styles.training.scss";
import {InputsTextArea} from "utils/inputs/inputs";
import {InputsImage} from "utils/inputs/inputs";
import {InputsFile} from "utils/inputs/inputs";
import {ButtonForms} from "utils/inputs/inputs";
import {InputsText} from "utils/inputs/inputs";

const FormAssociation = ({updateMember}) => {
  const [club, setClub] = useState(defaultStateClub);
  const [picture, setPicture] = useState({name_picture: "", alt_picture: ""});
  let [NAME_PICTURE, setNAME_PICTURE] = useState("");
  const [file, setFile] = useState([]);

  const onTextChange = (type, value) => {
    setClub({...club, [type]: value});
  };

  const handlePictChange = (type, value) => {
    setPicture({...picture, [type]: value});
  };

  const handleChangeFile = file => {
    setFile(file);
    setPicture({...picture, name_picture: "asso/"+file[0].name});
  };

  useEffect(() => {
    if (updateMember)
      ControllerStateUpdateClub(updateMember, (m, pict) => {
        setClub(m);
        setNAME_PICTURE(pict.name_picture);
        setPicture(pict);
      });
  }, [updateMember]);
  const handleSubmit = e => {
    e.preventDefault();
    let pict = {...club, picture: picture};
    const forms = new FormData();
    forms.append("membreAsso", JSON.stringify(pict));
    if (file[0]) {
      forms.append("files", file[0]);
    }
    if (updateMember) {
      putMember(updateMember.id_membre_asso, forms).then(res =>
        alert("Membre association a été mis à jour")
      );
    } else {
      postMember(forms).then(res => {
        alert("Membre association a bien été ajouté");
        window.location.reload();
      });
    }
  };

  return (
    <div>
      <InputsText
        label="Prénom :"
        type="firstname_membre_asso"
        defaultValue={club.firstname_membre_asso}
        action={(type, e) => onTextChange(type, e)}
      />
      <InputsText
        label="Nom :"
        type="lastname_membre_asso"
        defaultValue={club.lastname_membre_asso}
        action={(type, e) => onTextChange(type, e)}
      />
      <InputsText
        label="Numéro de téléphone :"
        type="phone_membre_asso"
        defaultValue={club.phone_membre_asso}
        action={(type, e) => onTextChange(type, e)}
      />
      <InputsText
        label="Mail :"
        type="mail_membre_asso"
        defaultValue={club.mail_membre_asso}
        action={(type, e) => onTextChange(type, e)}
      />
      <InputsTextArea
        label="Description :"
        type="description_membre_asso"
        defaultValue={club.description_membre_asso}
        action={(type, e) => onTextChange(type, e)}
      />

      {updateMember && file.length === 0 && (
        <InputsImage src={NAME_PICTURE} alt={picture.alt_picture} />
      )}
      {file[0] && (
        <img src={URL.createObjectURL(file[0])} alt="Images upload menbre asso" />
      )}

      <InputsFile label="Photo :" action={e => handleChangeFile(e)} />
      <InputsText
        label="Description de l'image :"
        type="alt_picture"
        defaultValue={picture.alt_picture}
        action={(type, e) => handlePictChange(type, e)}
      />
      <ButtonForms
        label="Annuler"
        action={e => window.location.assign("/admin/association")}
      />
      <ButtonForms
        label={updateMember ? "Modifier" : "Ajouter"}
        action={e => handleSubmit(e)}
      />
    </div>
  );
};

export default FormAssociation;

import React, {useState} from "react";
import ListRouteComponents from "components/admin/ListRouteComponents";
import {deleteMembersClub} from "controler/apiService/apiService.club";
import FormAssociation from "./association.formAssociation";
import ListAssociations from "./association.listAssociations";

const IndexAssociation = () => {
  const [edit, setEdit] = useState(false);
  const [member, setMember] = useState();

  const addMember = () => {
    setEdit(true);
  };

  const updateMember = memberToUpdate => {
    setMember(memberToUpdate);
    setEdit(true);
  };

  const deleteMember = memberToDelete => {
    deleteMembersClub(memberToDelete.id_membre_asso).then(res =>
      window.location.reload()
    );
  };

  const handleSubmit = e => {
    e.preventDefault();
    setEdit(false);
  };

  return (
    <div>
      <div>
        <ListRouteComponents />
      </div>
      {edit ? (
        <FormAssociation updateMember={member} action={e => handleSubmit(e)} />
      ) : (
        <ListAssociations
          addMember={() => addMember()}
          updateMember={memberToUpdate => updateMember(memberToUpdate)}
          deleteMember={memberToDelete => deleteMember(memberToDelete)}
        />
      )}
    </div>
  );
};

export default IndexAssociation;

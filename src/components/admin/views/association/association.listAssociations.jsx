import {getAllMembers} from "controler/apiService/apiService.club";
import React, {useEffect, useState} from "react";
import {ButtonForms} from "utils/inputs/inputs";

const ListAssociations = ({addMember, updateMember, deleteMember}) => {
  const [allMembers, setAllMembers] = useState([]);

  const deleteClubMember = value => {
    deleteMember(value);
    getAllMembers().then(res => {
      setAllMembers(res);
    });
  };

  useEffect(() => {
    let isMounted = false;
    getAllMembers().then(res => {
      if (!isMounted) setAllMembers(res);
    });
    return () => {
      isMounted = true;
    };
  }, []);

  return (
    <div>
      <ButtonForms label="Nouveau Membre" action={e => addMember()} />
      <h1>Liste des membres :</h1>
      {allMembers &&
        allMembers.map((value, key) => {
          return (
            <div key={key}>
              {value.firstname_membre_asso} {value.lastname_membre_asso}
              <ButtonForms label="éditer" action={e => updateMember(value)} />
              <ButtonForms label="X" action={e => deleteClubMember(value)} />
            </div>
          );
        })}
    </div>
  );
};
export default ListAssociations;

import {DefaultSup} from "controler/controller.data.team";
import React, {useState, useEffect} from "react";
import {InputsFile, InputsText, InputsImage} from "utils/inputs/inputs";

const FormSupervisor = ({supervisor, sendForm}) => {
  const [state, setState] = useState(DefaultSup);
  const [file, setFile] = useState([]);

  useEffect(() => {
    setState(supervisor);
  }, [supervisor]);

  useEffect(() => {
    if (sendForm.send) {
      sendForm.action({...state}, file);
    }
  }, [file, sendForm, state]);

  const handleChangeSupervisor = (type, value) => {
    setState({...state, [type]: value});
  };

  const handleChangeFile = (type, e) => {
    switch (type) {
      case "fileSup1":
        setState({
          ...state,
          pic_supervisor1: {...state.pic_supervisor1, name_picture: "teams/" + e[0].name},
        });
        break;
      case "fileSup2":
        setState({
          ...state,
          pic_supervisor2: {...state.pic_supervisor2, name_picture: "teams/" + e[0].name},
        });
        break;
      case "fileSup3":
        setState({
          ...state,
          pic_supervisor3: {...state.pic_supervisor3, name_picture: "teams/" + e[0].name},
        });
        break;
      default:
        break;
    }
    setFile({...file, [type]: e[0]});
  };

  return (
    <div>
      {/* ENCADRANT 1 */}
      <div>
        <p>Encadrant 1</p>
        <InputsText
          type="supervisor_1"
          defaultValue={state.supervisor_1}
          action={(type, e) => handleChangeSupervisor(type, e)}
        />
        <p>Fonction 1</p>
        <InputsText
          type="function_supervisor1"
          defaultValue={state.function_supervisor1}
          action={(type, e) => handleChangeSupervisor(type, e)}
        />
        {state.pic_supervisor1 !== null &&
          file.fileSup1 === undefined &&
          state.pic_supervisor1.name_picture !== "" && (
            <InputsImage
              src={state.pic_supervisor1.name_picture}
              alt={state.pic_supervisor1.alt_picture}
            />
          )}
        {file.fileSup1 && (
          <img
            src={URL.createObjectURL(file.fileSup1)}
            alt="Images upload supervisor 1"
          />
        )}
        <InputsFile
          label="Images Encadrant 1"
          action={e => handleChangeFile("fileSup1", e)}
        />
      </div>
      {/* ENCADRANT 2 */}
      <div>
        <p>Encadrant 2</p>
        <InputsText
          type="supervisor_2"
          defaultValue={state.supervisor_2}
          action={(type, e) => handleChangeSupervisor(type, e)}
        />
        <p>Fonction 2</p>
        <InputsText
          type="function_supervisor2"
          defaultValue={state.function_supervisor2}
          action={(type, e) => handleChangeSupervisor(type, e)}
        />
        {state.pic_supervisor2 !== null &&
          file.fileSup2 === undefined &&
          state.pic_supervisor2.name_picture !== "" && (
            <InputsImage
              src={state.pic_supervisor2.name_picture}
              alt={state.pic_supervisor2.alt_picture}
            />
          )}
        {file.fileSup2 && (
          <img
            src={URL.createObjectURL(file.fileSup2)}
            alt="Images upload supervisor 2"
          />
        )}
        <InputsFile
          label="Images Encadrant 2"
          action={e => handleChangeFile("fileSup2", e)}
        />
      </div>
      {/* ENCADRANT 3 */}
      <div>
        <p>Encadrant 3</p>
        <InputsText
          type="supervisor_3"
          defaultValue={state.supervisor_3}
          action={(type, e) => handleChangeSupervisor(type, e)}
        />
        <p>Fonction 3</p>
        <InputsText
          type="function_supervisor3"
          defaultValue={state.function_supervisor3}
          action={(type, e) => handleChangeSupervisor(type, e)}
        />
        {state.pic_supervisor3 !== null &&
          file.fileSup3 === undefined &&
          state.pic_supervisor3.name_picture !== "" && (
            <InputsImage
              src={state.pic_supervisor3.name_picture}
              alt={state.pic_supervisor3.alt_picture}
            />
          )}
        {file.fileSup3 && (
          <img
            src={URL.createObjectURL(file.fileSup3)}
            alt="Images upload supervisor 1"
          />
        )}
        <InputsFile
          label="Images Encadrant 3"
          action={e => handleChangeFile("fileSup3", e)}
        />
      </div>
    </div>
  );
};

export default FormSupervisor;

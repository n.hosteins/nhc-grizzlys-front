import ListRouteComponents from "components/admin/ListRouteComponents";
import React, {useState, useEffect} from "react";
import {Link} from "react-router-dom";
import {ButtonForms} from "utils/inputs/inputs";
import {deleteTeam, getAllTeams, putTeam} from "controler/apiService/apiService.Teams";
import {DragDropContext, Droppable, Draggable} from "react-beautiful-dnd";

const IndexTeam = () => {
  const [allTeam, setAllTeam] = useState([]);

  useEffect(() => {
    getAllTeams().then(res => {
      setAllTeam(res);
    });
  }, []);

  const delTeam = id => {
    deleteTeam(id).then(res => {
      alert("L'équipe a bien étè suprimer");
      window.location.reload();
    });
  };

  const ondragend = result => {
    const {destination, source, reason} = result;
    if (!destination || reason === "CANCEL") {
      return;
    }
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }
    let i = allTeam.find((el, key) => key === source.index);
    i.priority = destination.index;
    let a = allTeam.find((el, key) => key === destination.index);
    a.priority = source.index;
    const form1 = new FormData();
    const form2 = new FormData();
    form1.append("team", JSON.stringify(a));
    form2.append("team", JSON.stringify(i));
    putTeam(a.id_team_teams, form1);
    putTeam(i.id_team_teams, form2);
    const users = Object.assign([], allTeam);
    const droppedUser = allTeam[source.index];
    users.splice(source.index, 1);
    users.splice(destination.index, 0, droppedUser);
    setAllTeam(users);
  };

  return (
    <div>
      <div>
        <ListRouteComponents />
      </div>
      <div>
        <div>
          <Link to="/admin/teams/addTeam">Ajouter une équipe</Link>
        </div>
        <div>
          <h1>Gestion des équipes</h1>
          <div style={{display: "flex", justifyContent: "center", height: "100%"}}>
            <DragDropContext onDragEnd={result => ondragend(result)}>
              <div className="container">
                <div className="users">
                  <h1>users</h1>
                  <Droppable droppableId="dp1">
                    {provided => (
                      <div ref={provided.innerRef} {...provided.droppableProps}>
                        {allTeam &&
                          allTeam.map((value, key) => {
                            return (
                              <Draggable key={key} draggableId={key + ""} index={key}>
                                {provided => (
                                  <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                  >
                                    <span>{value.name_team}</span>
                                    <Link to={"/admin/teams/edit/" + value.id_team_teams}>
                                      Editer
                                    </Link>
                                    <ButtonForms
                                      label={"X"}
                                      action={e => delTeam(value.id_team_teams)}
                                    />
                                  </div>
                                )}
                              </Draggable>
                            );
                          })}
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                </div>
              </div>
            </DragDropContext>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IndexTeam;

import React, {useState, useEffect} from "react";
import {Link, useParams} from "react-router-dom";
import ListRouteComponents from "components/admin/ListRouteComponents";
import {Form} from "react-bootstrap";
import {
  ControllerStateUpdateTeam,
  DefaultSup,
  DefaultTeam,
} from "controler/controller.data.team";
import {getOneTeam, postTeam, putTeam} from "controler/apiService/apiService.Teams";
import {ButtonForms, InputsImage, InputsText, InputsFile} from "utils/inputs/inputs";
import FormSupervisor from "./formulaires/team.formulaires.formSupervisor";
import Wysiwyg from "utils/inputs/wysiwyg";

const TeamFormIndex = () => {
  const [team, setTeam] = useState(DefaultTeam);
  const [supervisor, setSupervisor] = useState(DefaultSup);
  const [sendForm, setSendFrom] = useState({send: false});
  const [file, setFile] = useState(null);
  const [presentation, setPresentation] = useState("");
  let {id} = useParams();

  useEffect(() => {
    if (id) {
      getOneTeam(id).then(res => {
        ControllerStateUpdateTeam(res, (t, s) => {
          setTeam(t);
          setSupervisor(s);
        });
      });
    }
  }, [id]);

  const handleChange = (type, key, value) => {
    switch (type) {
      case "changeTeam":
        setTeam({...team, [key]: value});
        break;
      case "changePresentation":
        setPresentation(value);
        break;
      case "changePicture":
        setTeam({...team, picture: {...team.picture, [key]: value}});
        break;
      case "changeFile":
        setFile(value[0]);
        handleChange("changePicture", "name_picture", "teams/" + value[0].name);
        break;
      default:
        break;
    }
  };

  const handleSubmit = e => {
    e.preventDefault();
    setSendFrom({send: true, action: (data, images) => verifieData(data, images)});
  };

  const verifieData = (data, images) => {
    const forms = new FormData();
    let sub = {
      ...team,
      presentation_team: presentation === "" ? team.presentation_team : presentation,
      ...data,
    };

    /** AJOUTE LES DONNEES A FORMS */
    forms.append("team", JSON.stringify(sub));

    /** AJOUTE LES FICHIERS A FORMS */
    if (images.fileSup1) {
      forms.append("fileSup1", images.fileSup1);
    }
    if (images.fileSup2) {
      forms.append("fileSup2", images.fileSup2);
    }
    if (images.fileSup3) {
      forms.append("fileSup3", images.fileSup3);
    }

    /** PUOT POUR MODIFIER UNE EQUIPE */
    if (id) {
      if (file) {
        forms.append("files", file);
      }
      putTeam(id, forms).then(res => alert("L'équipe a été modifié"));
    } else {
      // POST POUR AJOUTE UNE EQUIPE
      if (file === null) {
        alert("Ajouter une image");
      } else {
        forms.append("files", file);
        postTeam(forms).then(res => {
          alert("Une nouvelle equipe a été ajouté");
          window.location.reload();
        });
      }
    }
  };

  return (
    <>
      <div>
        <ListRouteComponents />
      </div>
      <p>
        <Link to="/admin/teams">{"<"}</Link>
      </p>
      <h1>
        <Link to="/admin/teams">Gestion des équipes</Link>
        {" > "} {id ? team.name_team : "Ajouter une équipe"}
      </h1>
      <Form style={{width: "50%", margin: "0 auto"}}>
        {/* FORM TEAM */}
        <div>
          <InputsText
            label="Titre"
            defaultValue={team.name_team}
            type="name_team"
            action={(key, value) => handleChange("changeTeam", key, value)}
          />
          <Wysiwyg
            labvaluel="Contenu"
            defaultValue={team.presentation_team}
            type="presentation_team"
            action={html => handleChange("changePresentation", "article", html)}
          />
        </div>

        {/* IMAGES */}
        <div>
          {team.picture.name_picture !== "" && file === null && (
            <>
              <InputsImage
                src={team.picture.name_picture}
                alt={team.picture.alt_picture}
              />
            </>
          )}
          {file && (
            <img src={URL.createObjectURL(file)} alt="Images upload menbre asso" />
          )}
          <InputsFile
            label="Images"
            action={value => handleChange("changeFile", "fileTeam", value)}
          />
          <InputsText
            label="Description de l'image"
            type="alt_picture"
            defaultValue={team.picture.alt_picture}
            action={(key, value) => handleChange("changePicture", key, value)}
          />
        </div>

        {/* FORM SUPERVISOR */}
        <FormSupervisor supervisor={supervisor} sendForm={sendForm} />

        {/* BUTTON */}

        <Link to="/admin/teams">Annuler</Link>

        <ButtonForms
          label={id ? "Enregistrer" : "Ajouter"}
          action={e => handleSubmit(e)}
        />
      </Form>
    </>
  );
};

export default TeamFormIndex;

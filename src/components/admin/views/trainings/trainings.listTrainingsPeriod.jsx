import React, {useEffect, useState} from "react";
import {getByPeriodTraining} from "controler/apiService/apiService.Trainings";
import "styles/users.styles/users.styles.training.scss";
import DaysAdmin from "./trainings.listTrainingsDays";
import TrainingForm from "./trainings.formTraining";
import {ButtonForms} from "utils/inputs/inputs";

const ListTrainingsPeriod = ({addTraining}) => {
  const [period, setPeriod] = useState("scolaire");
  const [listTraining, setListTraining] = useState([]);
  const [edit, setEdit] = useState(false);
  const [updateToTraining, setUpdateToTraining] = useState();

  useEffect(() => {
    getByPeriodTraining(period).then(res => {
      setListTraining(res);
    });
  }, [period]);

  const addNewTraining = addTraining => {
    setEdit(true);
  };

  const updateTraining = trainingToUpdate => {
    setEdit(true);
    setUpdateToTraining(trainingToUpdate);
  };
  return edit ? (
    <TrainingForm updateTraining={updateToTraining} />
  ) : (
    <>
      <ButtonForms label="Nouvel entraînement" action={e => addNewTraining(e)} />

      <button
        className={period === "scolaire" ? "period-active" : ""}
        onClick={() => setPeriod("scolaire")}
      >
        Période Scolaire
      </button>
      <button
        className={period === "vacances" ? "period-active" : ""}
        onClick={() => setPeriod("vacances")}
      >
        Vacances
      </button>
      {listTraining.length !== 0 &&
        listTraining.days.map((value, key) => {
          return (
            <div key={key}>
              <DaysAdmin
                key={value.day}
                day={value.day}
                trainings={value.trainings}
                action={valueTraining => updateTraining(valueTraining)}
              />
            </div>
          );
        })}
    </>
  );
};
export default ListTrainingsPeriod;

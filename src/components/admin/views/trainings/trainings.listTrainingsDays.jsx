import React from "react";
import "styles/users.styles/users.styles.training.scss";
import {deleteTraining} from "controler/apiService/apiService.Trainings";
import {ButtonForms} from "utils/inputs/inputs";
import {parsingHour} from "utils/utilis.parsingHour";

const ListTrainingsDays = ({day, trainings, action}) => {
  const delTraining = value => {
    deleteTraining(value.id_training).then(res => {
      alert(
        `l'entraînement du  ${value.day_training} à ${value.start_hour_training} a été supprimé`
      );
      window.location.reload();
    });
  };

  return (
    trainings.length !== 0 && (
      <div className="training-card">
        <h3 className="training-card-title">{day}</h3>
        {trainings.length !== 0 &&
          trainings.map((value, key) => {
            return (
              <div key={key}>
                Entraînement : {parsingHour(value.start_hour_training, "h")}
                <ButtonForms label="éditer" action={e => action(value)} />
                <ButtonForms label="X" action={e => delTraining(value)} />
              </div>
            );
          })}
      </div>
    )
  );
};

export default ListTrainingsDays;

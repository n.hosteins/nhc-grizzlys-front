import React, {useEffect, useState} from "react";
import "styles/users.styles/users.styles.training.scss";
import {postTraining, putTraining} from "controler/apiService/apiService.Trainings";
import CheckboxEquipes from "utils/inputs/checkboxEquipes";
import CheckboxCloackroom from "utils/inputs/checkboxCloackroom";
import {RadioBox, InputsTime} from "utils/inputs/inputs";
import {
  ControllerStateUpdateTraining,
  defaultStateTraining,
} from "controler/controler.data.training";
import {parsingHour} from "utils/utilis.parsingHour";
import {ButtonForms} from "utils/inputs/inputs";
import CheckBoxDays from "utils/inputs/checkboxDays";

const FormTraining = ({updateTraining}) => {
  const [training, setTraining] = useState(defaultStateTraining);
  let [cloackroomToTab, setCloackroomToTab] = useState();

  useEffect(() => {
    //gestion update/add
    if (updateTraining) {
      ControllerStateUpdateTraining(updateTraining, training => {
        setTraining(training);
        setCloackroomToTab(training.cloakroom_training.split("_"));
      });
    }
  }, [updateTraining]);

  //gestion affichage vestiaires
  const vest = vestiaire => {
    let cloackroomInString = "";
    if (vestiaire.length === 1) {
      cloackroomInString = vestiaire[0];
    } else if (vestiaire.length === 2) {
      cloackroomInString = "A_B";
    }

    setTraining({...training, cloakroom_training: cloackroomInString});
  };

  //gestion radiobox
  const onRadioPeriodclick = e => {
    setTraining({...training, period: e});
  };

  const onRadioDayclick = e => {
    setTraining({...training, day_training: e});
  };

  const callB = value => {
    setTraining({...training, teams: value});
  };

  const handleSubmit = () => {
    //gestion periode
    if (training.period === "") {
      alert("veuillez selectionner une période");
    } else {
      if (updateTraining) {
        putTraining(updateTraining.id_training, training).then(res =>
          alert("Entrainement mis à jour")
        );
      } else {
        if (training.start_hour_training === "") {
          training.start_hour_training = "0000";
        }
        if (training.end_hour_training === "") {
          training.end_hour_training = "0000";
        }

        postTraining(training).then(res => {
          alert("Entrainement ajouté");
          window.location.reload();
        });
      }
    }
  };

  const onTimeChangeHandler = (type, value) => {
    let timeformated = value.split(":").join("");
    switch (type) {
      case "start":
        setTraining({...training, start_hour_training: timeformated});
        break;
      case "end":
        setTraining({...training, end_hour_training: timeformated});
        break;
      default:
        break;
    }
  };

  return (
    <>
      <div>
        Période :
        <RadioBox
          label="Période scolaire"
          value="scolaire"
          id="scolaire"
          name="periode"
          isChecked={training.period.includes("scolaire") && true}
          action={e => onRadioPeriodclick(e)}
        />
        <RadioBox
          label="Vacances"
          value="vacances"
          id="vacances"
          name="periode"
          isChecked={training.period.includes("vacances") && true}
          action={e => onRadioPeriodclick(e)}
        />
      </div>
      <div>
        <CheckBoxDays
          day_training={training.day_training}
          action={e => onRadioDayclick(e)}
        />
      </div>
      <div>
        <InputsTime
          defaultValue={parsingHour(training.start_hour_training, ":")}
          label="Heure de début : "
          type="start"
          action={(type, e) => onTimeChangeHandler(type, e.target.value)}
        />
        <InputsTime
          defaultValue={parsingHour(training.end_hour_training, ":")}
          label="Heure de fin : "
          type="end"
          action={(type, e) => onTimeChangeHandler(type, e.target.value)}
        />
      </div>
      <div>
        Vestiaire(s) :
        <CheckboxCloackroom
          returnValue={value => vest(value)}
          cloackroomSelected={cloackroomToTab}
        />
      </div>
      <div>
        Equipe(s) :
        <CheckboxEquipes
          teamSelected={training.teams}
          returnValue={value => callB(value)}
        />
      </div>
      <ButtonForms
        label="Annuler"
        action={e => window.location.assign("/admin/trainings")}
      />
      <ButtonForms label="Valider" action={() => handleSubmit()} />
    </>
  );
};

export default FormTraining;

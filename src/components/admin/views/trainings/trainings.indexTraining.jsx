import React, {useState} from "react";
import ListRouteComponents from "components/admin/ListRouteComponents";
import ListTrainingsPeriod from "./trainings.listTrainingsPeriod";

const IndexTraining = () => {
  const [setEdit] = useState(false);
  const addTraining = () => {
    setEdit(true);
  };

  return (
    <div>
      <ListRouteComponents />

      <ListTrainingsPeriod addTraining={() => addTraining()} />
    </div>
  );
};
export default IndexTraining;

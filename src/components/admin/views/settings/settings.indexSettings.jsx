import React from "react";
import ListRouteComponents from "components/admin/ListRouteComponents";
import {InputsText, InputsTextArea, ButtonForms} from "../../../../utils/inputs/inputs";
import {useState} from "react";
import {updateContact} from "controler/apiService/apiService.contact";
const IndexSetting = () => {
  const [state, setState] = useState({
    email_contact: "",
    address_asso: "",
    phone_asso: "",
    president_asso: "",
    president_text: "",
    meta_title: "",
    meta_text: "",
    picture: null,
  });

  const handleChange = (type, e) => {
    if (type === "meta_text" || "address_asso") {
      setState({...state, [type]: e.replace(/(\r\n|\n|\r)/gm, "\\n")});
    } else {
      setState({...state, [type]: e});
    }
  };

  const handleSubmit = () => {
    const forms = new FormData();
    forms.append("params", JSON.stringify(state));
    updateContact(forms);
  };

  return (
    <div>
      <div>
        <ListRouteComponents />
      </div>
      <div>
        <form>
          <span>Info de contact </span>
          <small>
            Cet adresse email recevra les messages envoyés à partir du formulaire de
            contact
          </small>
          <InputsText
            label={"Adresse email de contact"}
            action={(type, e) => handleChange("email_contact", e)}
          />
          <InputsTextArea
            label={"Adresse postale de l'association"}
            action={(type, e) => handleChange("address_asso", e)}
          />
          <InputsText
            label={"Numéro de téléphone de l'association"}
            action={(type, e) => handleChange("phone_asso", e)}
          />
          <InputsText
            label={"Nom du Président de l'association"}
            action={(type, e) => handleChange("president_asso", e)}
          />
        </form>
        <form>
          <span>Meta-données </span>
          <small>
            Les méta-données sont utilisées pour optimiser le référencement de site sur
            les moteurs de recherche
          </small>
          <InputsText
            label={"Titre du site"}
            action={(type, e) => handleChange("meta_title", e)}
          />
          <InputsTextArea
            label={"Extrait"}
            action={(type, e) => handleChange("meta_text", e)}
          />
        </form>
        <ButtonForms label={"Annuler"} action={e => window.location.assign("/admin")} />
        <ButtonForms label={"Enregistrer"} action={e => handleSubmit()} />
      </div>
    </div>
  );
};

export default IndexSetting;

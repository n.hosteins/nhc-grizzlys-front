import React, {useState} from "react";
import "styles/admin.styles/admin.partner.styles.scss";
import ListRouteComponents from "components/admin/ListRouteComponents";
import {deletePartner} from "controler/apiService/apiService.Partners";
import FormPartner from "./partner.formPartner";
import ListPartners from "./partner.listPartners";

const IndexPartner = () => {
  const [editMode, setEditMode] = useState(false);
  const [partner, setPartner] = useState();

  const addPartner = () => {
    setEditMode(true);
  };

  const delPartner = (e, id) => {
    e.preventDefault();
    deletePartner(id).then(res => {
      alert("Le partenaire a été supprimé");
      window.location.reload();
    });
  };

  const updatePartner = PartnerToUpdate => {
    setPartner(PartnerToUpdate);
    setEditMode(true);
  };

  const handleSubmit = e => {
    e.preventDefault();
    setEditMode(false);
  };
  return (
    <div className="admin-partenaire">
      <div>
        <ListRouteComponents />
      </div>
      <div className="admin-partenaire-content">
        <div className="admin-news-content">
          {editMode ? (
            <FormPartner updateToPartner={partner} action={e => handleSubmit(e)} />
          ) : (
            <ListPartners
              addPartner={() => addPartner()}
              deletePartner={(e, id) => delPartner(e, id)}
              updatePartner={partenaireToUpdate => updatePartner(partenaireToUpdate)}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default IndexPartner;

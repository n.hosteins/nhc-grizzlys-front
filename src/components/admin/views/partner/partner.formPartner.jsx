import React, {useState, useEffect} from "react";
import {Form} from "react-bootstrap";
import {ButtonForms, InputsText, InputsFile} from "utils/inputs/inputs";
import Wysiwyg from "utils/inputs/wysiwyg";
import {
  ControllerStateUpdatePartner,
  defaultStatePartner,
} from "controler/control.data.partner";
import {addPartner, updatePartner} from "controler/apiService/apiService.Partners";
import {defaultStatePicture} from "controler/controller.data.picture";
import {InputsImage} from "utils/inputs/inputs";

const FormPartner = ({updateToPartner}) => {
  const [partenaire, setPartner] = useState(defaultStatePartner);
  const [picture, setPicture] = useState(defaultStatePicture);
  const [descritpion, setDescription] = useState("");
  let [NAME_PICTURE, setNAME_PICTURE] = useState("");

  useEffect(() => {
    if (updateToPartner)
      ControllerStateUpdatePartner(updateToPartner, (partenaire, picture) => {
        setPartner(partenaire);
        setPicture(picture);
        setDescription(partenaire.description_partner);
        setNAME_PICTURE(picture.name_picture);
      });
  }, [updateToPartner]);

  const [file, setFile] = useState([]);

  const handleChangePartner = (type, value) => {
    setPartner({...partenaire, [type]: value});
  };
  const handleChangePicture = (type, value) => {
    setPicture({...picture, [type]: value});
  };
  const handleChangeDescription = html => {
    setDescription(html);
  };

  const handleChangeFile = file => {
    setFile(file);
    setPicture({...picture, name_picture: "partners/"+file[0].name});
  };

  const handleSubmit = e => {
    let sub = {
      ...partenaire,
      description_partner: descritpion,
      picture: picture,
    };
    const forms = new FormData();
    forms.append("partenaire", JSON.stringify(sub));
    if (file[0]) {
      forms.append("files", file[0]);
    }
    if (updateToPartner) {
      updatePartner(updateToPartner.id_partner, forms).then(res =>
        alert("Partenaire a été mis à jour")
      );
    } else {
      if (partenaire !== defaultStatePartner) {
        addPartner(forms).then(res => {
          alert("Partenaire bien ajouté");
          window.location.reload();
        });
      } else {
        alert("Veuillez renseigner tous les champs !");
      }
    }
  };

  return (
    <Form style={{width: "80%", margin: "0 auto"}}>
      <InputsText
        label="Nom partenaire"
        defaultValue={partenaire.name_partner}
        type="name_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      <Wysiwyg
        label="Description partenaire"
        defaultValue={partenaire.description_partner}
        type="description_partner"
        action={html => handleChangeDescription(html)}
      />
      <InputsText
        label="Mail partenaire"
        defaultValue={partenaire.mail_partner}
        type="mail_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      <InputsText
        label="Phone partenaire"
        defaultValue={partenaire.phone_partner}
        type="phone_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      <InputsText
        label="Rue partenaire"
        defaultValue={partenaire.street_partner}
        type="street_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      <InputsText
        label="Ville partenaire"
        defaultValue={partenaire.city_partner}
        type="city_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      <InputsText
        label="Code postal partenaire"
        defaultValue={partenaire.zip_partner}
        type="zip_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      <InputsText
        label="Site partenaire"
        defaultValue={partenaire.website_partner}
        type="website_partner"
        action={(type, e) => handleChangePartner(type, e)}
      />
      {updateToPartner && file.length === 0 && (
        <InputsImage src={NAME_PICTURE} alt={picture.alt_picture} />
      )}
      {file.length !== 0 && (
        <img src={URL.createObjectURL(file[0])} alt="Images upload partenaires" />
      )}
      <InputsFile label="Images partenaire" action={e => handleChangeFile(e)} />
      <InputsText
        label="Description de l'image"
        type="alt_picture"
        defaultValue={picture.alt_picture}
        action={(type, e) => handleChangePicture(type, e)}
      />
      <ButtonForms
        label="Annuler"
        action={e => window.location.assign("/admin/partners")}
      />
      <ButtonForms
        label={updateToPartner ? "Enregistrer" : "Ajoutez "}
        action={e => handleSubmit(e)}
      />
    </Form>
  );
};

export default FormPartner;

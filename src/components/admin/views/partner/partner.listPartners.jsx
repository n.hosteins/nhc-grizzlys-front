import React, {useEffect, useState} from "react";
import {ButtonForms} from "utils/inputs/inputs";
import {getAllPartners} from "controler/apiService/apiService.Partners";

const ListPartners = ({addPartner, deletePartner, updatePartner}) => {
  const [allPartners, setAllPartners] = useState([]);

  useEffect(() => {
    getAllPartners().then(res => setAllPartners(res));
  }, [addPartner]);

  return (
    <div>
      <div>
        <ButtonForms label="Ajouter un partenaire" action={e => addPartner()} />
      </div>
      <div>
        <h1>Liste des partenaires</h1>
        {allPartners &&
          allPartners.map((value, key) => {
            return (
              <div key={key}>
                <span>{value.name_partner}</span>
                <ButtonForms label={"editer"} action={e => updatePartner(value)} />
                <ButtonForms
                  label={"delete"}
                  action={e => deletePartner(e, value.id_partner)}
                />
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default ListPartners;

import React from "react";
import "styles/admin.styles/admin.styles.scss";
import {Link} from "react-router-dom";

const AdminHome = () => {
  return (
    <>
      <ul className="navbar-nav mr-auto admin-menu">
        <li>
          <Link to={"/admin/teams"} className="nav-link admin-menu-link">
            <img src={require("assets/images/group-of-people.svg")} alt="" />
            Gérer les équipes
          </Link>
        </li>
        <li>
          <Link to={"/admin/trainings"} className="nav-link admin-menu-link">
            <img src={require("assets/images/strong.svg")} alt="" />
            Gérer les entraînements
          </Link>
        </li>
        <li>
          <Link to={"/admin/matchs"} className="nav-link admin-menu-link">
            <img src={require("assets/images/player.svg")} alt="" />
            Gérer les matchs
          </Link>
        </li>
        <li>
          <Link to={"/admin/news"} className="nav-link admin-menu-link">
            <img src={require("assets/images/newspaper.svg")} alt="" />
            Gérer les actualités
          </Link>
        </li>
        <li>
          <Link to={"/admin/association"} className="nav-link admin-menu-link">
            <img src={require("assets/images/interaction.svg")} alt="" />
            Gérer l'association
          </Link>
        </li>
        <li>
          <Link to={"/admin/partners"} className="nav-link admin-menu-link">
            <img src={require("assets/images/briefcase.svg")} alt="" />
            Gérer les partenaires
          </Link>
        </li>
        <li>
          <Link to={"/admin/settings"} className="nav-link admin-menu-link">
            <img src={require("assets/images/briefcase.svg")} alt="" />
            Réglage du site
          </Link>
        </li>
      </ul>
    </>
  );
};

export default AdminHome;

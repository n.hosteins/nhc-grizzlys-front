import React from "react";
import ListRouteComponents from "../../ListRouteComponents";
import NewsList from "./news.listNews";
import "styles/admin.styles/admin.news.styles.scss";

const IndexNews = () => {
  return (
    <div className="admin-news">
      <div>
        <ListRouteComponents />
      </div>
      <div className="admin-news-content">
        <h1>Gestion des actualités</h1>
        <div>
          <NewsList />
        </div>
      </div>
    </div>
  );
};

export default IndexNews;

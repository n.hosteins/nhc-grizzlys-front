import {
  get20news,
  getNewsByTitle,
  deleteNews,
} from "controler/apiService/apiService.News";
import React, {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import {ButtonForms, InputsText} from "utils/inputs/inputs";

const ListNews = ({addNews, updateNews}) => {
  const [allNews, setAllNews] = useState([]);
  const [indexNews, setIndexNews] = useState(0);
  const [isLast, setIsLast] = useState(true);
  const [searchNews, setSearchNews] = useState("");

  useEffect(() => {
    get20news(indexNews).then(e => {
      setAllNews(e.news);
      setIsLast(e.newsLeft);
    });
  }, [indexNews, addNews]);

  const handleSearchNews = value => {
    setSearchNews(value);
    if (searchNews.length >= 2) {
      getNewsByTitle(searchNews).then(res => setAllNews(res));
    } else {
      get20news(indexNews).then(e => {
        setAllNews(e.news);
        setIsLast(e.newsLeft);
      });
    }
  };

  const deleteNewsById = id => {
    deleteNews(id).then(res => {
      alert("L'article a été supprimé");
      window.location.reload();
    });
  };

  return (
    <div>
      <InputsText
        label="Rechercher une actualité ?"
        defaultValue={searchNews}
        type="rechercher"
        action={(type, e) => handleSearchNews(e)}
      />
      <div>
        <Link to="/admin/news/addNews">Ajouter une actualité</Link>
      </div>
      <div>
        {allNews &&
          allNews.map((value, key) => {
            return (
              <div key={key}>
                <span>{value.title_news}</span>
                <span>
                  {"fonction générique ici "}
                  {value.date_news
                    .split("T")[0]
                    .split("-")
                    .reverse()
                    .join("-")}
                </span>
                <Link to={"/admin/news/edit/" + value.id_news}>Editer</Link>

                <ButtonForms
                  label={"supprimer"}
                  action={e => deleteNewsById(value.id_news)}
                />
              </div>
            );
          })}
        {indexNews !== 0 && (
          <button onClick={() => setIndexNews(indexNews - 10)}>Precedent</button>
        )}
        {isLast && <button onClick={() => setIndexNews(indexNews + 10)}>Suivant</button>}
      </div>
    </div>
  );
};

export default ListNews;

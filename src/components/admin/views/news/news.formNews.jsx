import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import {ButtonForms, InputsText, InputsFile, InputsImage} from "utils/inputs/inputs";
import {postNews, updateNews, getOneNews} from "controler/apiService/apiService.News";
import Wysiwyg from "utils/inputs/wysiwyg";
import {ControllerStateUpdateNews, defaultStateNews} from "controler/control.data.news";
import {defaultStatePicture} from "controler/controller.data.picture";
import ListRouteComponents from "components/admin/ListRouteComponents";
import {Link, useParams} from "react-router-dom";
import CheckboxEquipes from "utils/inputs/checkboxEquipes";

const FormNews = () => {
  const [news, setNews] = useState(defaultStateNews);
  const [tags, setTags] = useState([]);
  const [picture, setPicture] = useState(defaultStatePicture);
  const [article, setArticle] = useState("");
  let [NAME_PICTURE, setNAME_PICTURE] = useState("");
  let {id} = useParams();

  useEffect(() => {
    if (id) {
      getOneNews(id).then(res => {
        ControllerStateUpdateNews(res, (n, p, t) => {
          setNews(n);
          setPicture(p);
          setTags(t);
          setNAME_PICTURE(p.name_picture);
          setArticle(n.article_news);
        });
      });
    }
  }, [id]);

  const [file, setFile] = useState([]);
  const handleChangeNews = (type, value) => {
    setNews({...news, [type]: value});
  };
  const handleChangePicture = (type, value) => {
    setPicture({...picture, [type]: value});
  };

  const handleChangeFile = file => {
    setFile(file);
    setPicture({...picture, name_picture: "news/" + file[0].name});
  };

  const handleChangeArticle = html => {
    setArticle(html);
  };

  const handleSubmit = e => {
    e.preventDefault();
    let sub = {
      ...news,
      article_news: article,
      picture: picture,
    };
    const forms = new FormData();
    forms.append("news", JSON.stringify(sub));
    forms.append("tags", JSON.stringify({teams: tags}));
    if (file[0]) {
      forms.append("files", file[0]);
    }
    if (id) {
      updateNews(id, forms).then(res => alert("L'actualité a bien été mis à jour "));
    } else {
      postNews(forms).then(res => {
        alert("L'actualité a bien été ajouté");
        window.location.reload();
      });
    }
  };

  const hangleChangeTags = e => {
    setTags(e);
  };

  return (
    <div>
      <div>
        <ListRouteComponents />
      </div>

      <Link to="/admin/news">{"<"}</Link>
      <h1>
        <Link to="/admin/news">Gestion des actualités</Link>
        {" > "} {id ? news.title_news : "Ajouter une actualité"}
      </h1>
      <Form style={{width: "100%", margin: "0 auto"}}>
        <InputsText
          label="Titre"
          defaultValue={news.title_news}
          type="title_news"
          action={(type, e) => handleChangeNews(type, e)}
        />
        <div>
          <CheckboxEquipes teamSelected={tags} returnValue={e => hangleChangeTags(e)} />
        </div>
        <Wysiwyg
          label="Contenu"
          defaultValue={news.article_news}
          action={html => handleChangeArticle(html)}
        />
        {id && file.length === 0 && (
          <InputsImage src={NAME_PICTURE} alt={picture.alt_picture} />
        )}
        {file[0] && (
          <img
            src={URL.createObjectURL(file[0])}
            alt="Images upload membre association"
          />
        )}
        <InputsFile label="Images" action={e => handleChangeFile(e)} />
        <InputsText
          label="Description de l'image :"
          type="alt_picture"
          defaultValue={picture.alt_picture}
          action={(type, e) => handleChangePicture(type, e)}
        />
        <Link to="/admin/news">Annuler</Link>
        <ButtonForms
          label={id ? "Enregistrer" : "Ajoutez l'article"}
          action={e => handleSubmit(e)}
        />
      </Form>
    </div>
  );
};

export default FormNews;

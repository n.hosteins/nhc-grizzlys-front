import React, {useState, useEffect, useRef} from "react";
import {getAllTeams} from "controler/apiService/apiService.Teams";
import {parsingHour} from "utils/utilis.parsingHour";
import {deleteMatch, updateMatch} from "controler/apiService/apiService.match";
import {InputsTime, InputsText, ButtonForms} from "utils/inputs/inputs";

const ListMatch = ({data}) => {
  const [teams, setTeams] = useState([]);
  const [state, setState] = useState({
    date_match: data.date_match,
    hour_match: data.hour_match,
    location: data.location,
    title: data.title,
    team: data.team,
  });
  const _isMounted = useRef(true);
  useEffect(() => {
    getAllTeams()
      .then(res => {
        if (_isMounted.current) {
          setTeams(res);
        }
      })
      .catch(err => console.log(err));

    if (data) {
      setState(data);
    }
    return () => {
      _isMounted.current = false;
    };
  }, [data]);

  const selectHandler = e => {
    let selectedTeam = teams.filter(team => team.name_team === e.target.value);
    setState({...state, team: selectedTeam[0]});
  };
  const handleSubmit = e => {
    const forms = new FormData();
    forms.append("match", JSON.stringify(state));
    updateMatch(data.id_match, forms);
  };
  const handleChange = (type, e) => {
    setState({...state, [type]: e});
  };
  const deleteMatchByID = id => {
    deleteMatch(id).then(res => {
      alert(`le match ${id} a été supprimé"`);
      window.location.reload();
    });
  };
  return (
    <form style={{display: "flex", textAlign: "center"}}>
      <input
        type="date"
        value={state.date_match}
        onChange={e => handleChange("date_match", e.target.value)}
      />
      <InputsTime
        style={{width: "20%"}}
        defaultValue={parsingHour(state.hour_match)}
        action={(type, e) =>
          handleChange("hour_match", Number(e.target.value.split(":").join("")))
        }
      />
      <InputsText
        style={{width: "20%"}}
        type="text"
        defaultValue={state.title}
        action={(type, e) => handleChange("title", e)}
      />
      <div>
        <select
          value={state.team.name_team}
          name="teams"
          id="teams"
          onChange={e => selectHandler(e)}
        >
          {teams.map((value, key) => {
            return (
              <option key={key} value={value.name_team}>
                {value.name_team}
              </option>
            );
          })}
        </select>
      </div>
      <InputsText
        style={{width: "20%"}}
        type="text"
        defaultValue={state.location}
        action={(type, e) => handleChange("location", e)}
      />
      <div style={{width: "20%", display: "flex"}}>
        <ButtonForms
          label={"X"}
          style={{width: "33%"}}
          action={() => deleteMatchByID(data.id_match)}
        />
        <ButtonForms
          type="submit"
          label={"S"}
          style={{width: "33%"}}
          action={e => handleSubmit(e)}
        />
      </div>
    </form>
  );
};

export default ListMatch;

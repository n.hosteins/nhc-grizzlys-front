import React, {useEffect, useState} from "react";
import Match from "./match.listMatch";
import {get20Match} from "controler/apiService/apiService.match";
import AddMatch from "./match.addMatch";
import {getAllTeams} from "controler/apiService/apiService.Teams";
import {useRef} from "react";
import ListRouteComponents from "components/admin/ListRouteComponents";

const IndexMatch = () => {
  const [nextMatch, setNextMatch] = useState(false);
  const [index, setIndex] = useState(0);
  const [sorted, setSorted] = useState();
  const [filtered, setFiltered] = useState();
  const [bool, setBool] = useState(false);
  const [teamsList, setTeamsList] = useState();
  const _isMounted = useRef(true);

  useEffect(() => {
    get20Match(index).then(res => {
      if (_isMounted.current) {
        setSorted(res.matchEntities);
        setNextMatch(res.matchLeft);
      }
    });

    return function cleanup() {
      _isMounted.current = false;
    };
  }, [index]);

  useEffect(() => {
    getAllTeams()
      .then(res => {
        setTeamsList(res);
      })
      .catch(err => console.log(err));
  }, []);

  const sortMatch = type => {
    let temp = [...sorted];
    if (type === "Date") {
      if (bool) {
        temp
          .sort(function(a, b) {
            return new Date(b.date_match) - new Date(a.date_match);
          })
          .reverse();
      } else {
        temp.sort(function(a, b) {
          return new Date(b.date_match) - new Date(a.date_match);
        });
      }
    } else if (type === "Match") {
      if (bool) {
        temp.sort(function compare(a, b) {
          if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
          if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
          return 0;
        });
      } else {
        temp
          .sort(function compare(a, b) {
            if (a.title.toLowerCase() < b.title.toLowerCase()) return -1;
            if (a.title.toLowerCase() > b.title.toLowerCase()) return 1;
            return 0;
          })
          .reverse();
      }
    } else if (type === "Lieu") {
      if (bool) {
        temp
          .sort(function compare(a, b) {
            if (a.location.toLowerCase() < b.location.toLowerCase()) return -1;
            if (a.location.toLowerCase() > b.location.toLowerCase()) return 1;
            return 0;
          })
          .reverse();
      } else {
        temp.sort(function compare(a, b) {
          if (a.location.toLowerCase() < b.location.toLowerCase()) return -1;
          if (a.location.toLowerCase() > b.location.toLowerCase()) return 1;
          return 0;
        });
      }
    }
    setSorted(temp);
    setBool(!bool);
  };

  const selectHandler = e => {
    let temp = [...sorted];
    if (e === "default value") {
      setFiltered(null);
    } else {
      let newArr = temp.filter(function(el) {
        return el.team.name_team === e;
      });
      setFiltered(newArr);
    }
  };
  return (
    <>
      <div>
        <div>
          <ListRouteComponents />
        </div>
        <div style={{display: "flex"}}>
          <div style={{width: "20%"}} onClick={e => sortMatch("Date")}>
            <h1>DATE</h1>
          </div>
          <div style={{width: "20%"}}>HEURE</div>
          <div style={{width: "20%"}} onClick={e => sortMatch("Match")}>
            <h1>MATCH</h1>
          </div>
          <div style={{width: "20%"}}>
            <h1>EQUIPES</h1>
            <div>
              <select
                name="teams"
                id="teams"
                onChange={e => selectHandler(e.target.value)}
              >
                <option value={"default value"}>Sélectionner une équipe</option>

                {teamsList &&
                  teamsList.map((value, key) => {
                    return (
                      <option key={key} value={value.name_team}>
                        {value.name_team}
                      </option>
                    );
                  })}
              </select>
            </div>
          </div>
          <div style={{width: "20%"}} onClick={e => sortMatch("Lieu")}>
            <h1>LIEU</h1>
          </div>
          <div style={{width: "20%"}}>ACTION</div>
        </div>
        <div>
          <AddMatch />
          {sorted && !filtered
            ? sorted.map((value, key) => {
                return <Match key={key} data={value} />;
              })
            : filtered &&
              filtered.map((value, key) => {
                return <Match key={key} data={value} />;
              })}
        </div>
        <div>
          {index !== 0 && <h1 onClick={e => setIndex(index - 20)}>précédent</h1>}
          {nextMatch && <h1 onClick={e => setIndex(index + 20)}>suivant</h1>}
        </div>
      </div>
    </>
  );
};

export default IndexMatch;

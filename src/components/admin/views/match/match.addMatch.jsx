import React, {useEffect, useRef, useState} from "react";
import {getAllTeams} from "controler/apiService/apiService.Teams";
import {addMatch} from "controler/apiService/apiService.match";
import {parsingHour} from "utils/utilis.parsingHour";
import {InputsTime, InputsText} from "utils/inputs/inputs";

const AddMatch = () => {
  const [teams, setTeams] = useState([]);
  const _isMounted = useRef(true);

  useEffect(() => {
    getAllTeams().then(res => {
      if (_isMounted.current) {
        setTeams(res);
      }
    });
    return () => {
      _isMounted.current = false;
    };
  }, []);

  const [state, setState] = useState({
    date_match: "",
    hour_match: "--:--",
    location: "",
    title: "",
    team: {},
  });
  const handleChange = (e, type) => {
    setState({...state, [type]: e});
  };

  const selectHandler = e => {
    if (e.target.value === "default value") {
      alert(`Sélectionner une équipe s'il vous plaît`);
    } else {
      let selectedTeam = teams.filter(team => team.name_team === e.target.value);
      setState({...state, team: selectedTeam[0]});
    }
  };

  const convertDate = value => {
    setState({...state, date_match: value.split("/").join("-")});
  };

  const handleSubmit = () => {
    if (Object.keys(state.team).length === 0 && state.team.constructor === Object) {
      alert("Vous avez oublié de sélectionner une équipe");
    } else {
      const forms = new FormData();
      forms.append("match", JSON.stringify(state));
      addMatch(forms).then(res => {
        alert("Un match a été ajouté");
        window.location.reload();
      });
    }
  };

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        handleSubmit();
      }}
      style={{display: "flex", textAlign: "center"}}
    >
      <input
        required
        type="date"
        value={state.date_match}
        onChange={e => convertDate(e.target.value)}
      />
      <InputsTime
        style={{width: "20%"}}
        defaultValue={parsingHour(state.hour_match)}
        action={(type, e) =>
          handleChange(Number(e.target.value.split(":").join("")), "hour_match")
        }
      />
      <InputsText
        style={{width: "20%"}}
        type="text"
        defaultValue={state.title}
        action={(type, e) => handleChange(e, "title")}
      />
      <div>
        <select name="teams" id="teams" onChange={e => selectHandler(e)}>
          <option value={"default value"}>Sélectionner une équipe</option>
          {teams.map((value, key) => {
            return (
              <option key={key} value={value.name_team}>
                {value.name_team}
              </option>
            );
          })}
        </select>
      </div>
      <InputsText
        style={{width: "20%"}}
        type="text"
        defaultValue={state.location}
        action={(type, e) => handleChange(e, "location")}
      />
      <div style={{width: "20%", display: "flex"}}>
        <input type="submit" value={"S"} />
      </div>
    </form>
  );
};

export default AddMatch;

import React, {useEffect} from "react";
import "styles/admin.styles/admin.styles.scss";
import {Switch, Route} from "react-router-dom";
import News from "./views/news/news.indexNews";
import AdminHome from "components/admin/views/admin.home";
import Teams from "./views/team/team.indexForm";
import IndexPartner from "./views/partner/partner.indexPartner";
import Trainings from "./views/trainings/trainings.indexTraining";
import Match from "./views/match/match.indexMatch";
import ClubMembers from "./views/association/association.indexAssociation";
import IndexSettings from "./views/settings/settings.indexSettings";
import NotFound from "components/users/views/404/notFound";
import TeamFormIndex from "./views/team/team.formTeamIndex";
import NewsForm from "./views/news/news.formNews";

const Admin = () => {
  const [isMobile, setMobile] = React.useState(false);

  useEffect(() => {
    const userAgent = typeof window.navigator === "undefined" ? "" : navigator.userAgent;
    const mobile = Boolean(
      userAgent.match(
        /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i
      )
    );
    setMobile(mobile);
  }, []);

  return !isMobile ? (
    <div className="admin">
      <h1 className="admin-title">Administration</h1>
      <Switch>
        <Route exact path="/admin" component={AdminHome} />
        <Route exact path="/admin/teams" component={Teams} />
        <Route exact path="/admin/teams/edit/:id" children={<TeamFormIndex />} />
        <Route exact path="/admin/teams/addTeam" children={<TeamFormIndex />} />
        <Route exact path="/admin/news" component={News} />
        <Route exact path="/admin/news/edit/:id" children={<NewsForm />} />
        <Route exact path="/admin/news/addNews" children={<NewsForm />} />
        <Route exact path="/admin/partners" component={IndexPartner} />
        <Route exact path="/admin/trainings" component={Trainings} />
        <Route exact path="/admin/matchs" component={Match} />
        <Route exact path="/admin/association" component={ClubMembers} />
        <Route exact path="/admin/settings" component={IndexSettings} />

        <Route path="/admin/" component={NotFound} />
      </Switch>
    </div>
  ) : (
    <h1>PAGE NON AUTORISÉE VIA MOBILE</h1>
  );
};

export default Admin;

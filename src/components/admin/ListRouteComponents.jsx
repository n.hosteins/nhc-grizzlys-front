import React from "react";
import {Link} from "react-router-dom";
import "styles/admin.styles/admin.styles.scss";
const ListRouteComponents = () => {
  const LocationReload = reload => {
    window.location.assign(reload);
  };
  return (
    <ul className="navbar-nav mr-auto left-menu">
      <li>
        <Link
          onClick={() => LocationReload("/admin/teams")}
          to={"/admin/teams"}
          className="nav-link"
        >
          Équipes
        </Link>
      </li>
      <li>
        <Link
          onClick={() => LocationReload("/admin/trainings")}
          to={"/admin/trainings"}
          className="nav-link"
        >
          Entraînements
        </Link>
      </li>
      <li>
        <Link
          onClick={() => LocationReload("/admin/matchs")}
          to={"/admin/matchs"}
          className="nav-link"
        >
          Matchs
        </Link>
      </li>
      <li>
        <Link
          onClick={() => LocationReload("/admin/news")}
          to={"/admin/news"}
          className="nav-link"
        >
          News
        </Link>
      </li>
      <li>
        <Link
          onClick={() => LocationReload("/admin/association")}
          to={"/admin/association"}
          className="nav-link"
        >
          Association
        </Link>
      </li>
      <li>
        <Link
          onClick={() => LocationReload("/admin/partners")}
          to={"/admin/partners"}
          className="nav-link"
        >
          Partenaires
        </Link>
      </li>
      <li>
        <Link
          onClick={() => LocationReload("/admin/settings")}
          to={"/admin/settings"}
          className="nav-link"
        >
          Réglage du site
        </Link>
      </li>
    </ul>
  );
};

export default ListRouteComponents;

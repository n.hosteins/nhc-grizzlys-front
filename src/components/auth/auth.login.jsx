import React, {useState, useEffect} from "react";
import {Redirect} from "react-router-dom";
import {postLogin} from "controler/apiService/apiService.Login";
import {InputsText, ButtonForms} from "utils/inputs/inputs";
import Logo from "../../assets/images/logo.png";

export default function Login(props) {
  const {from} = props.location.state || {from: {pathname: "/"}};

  const [redirectToReferrer, setRedirectToReferrer] = useState(false);

  const [state, setState] = useState({
    username: "",
    password: "",
  });

  useEffect(() => {
    if (localStorage.getItem("token")) {
      setRedirectToReferrer(true);
    } else {
      setRedirectToReferrer(false);
    }
  }, []);

  const handleChange = (type, value) => {
    setState({...state, [type]: value});
  };

  const onSubmit = () => {
    postLogin(state).then(e => {
      localStorage.setItem("token", e);
      setRedirectToReferrer(true);
    });
  };

  if (redirectToReferrer) {
    return <Redirect to={from} />;
  }

  return (
    <>
      <form style={{textAlign: "center"}}>
        <img src={Logo} alt="logo-grizzly" />
        <InputsText
          label={"Username"}
          action={(type, e) => handleChange("username", e)}
        />
        <InputsText
          label={"password"}
          action={(type, e) => handleChange("password", e)}
        />
        <ButtonForms label={"Connexion"} action={e => onSubmit()} />
      </form>
    </>
  );
}

import React, {useEffect, useState} from "react";
import "styles/users.styles/users.styles.news.scss";
import {Card} from "react-bootstrap";
import {MDBIcon} from "mdbreact";
import {get5news} from "controler/apiService/apiService.News";
import {InputsImage} from "utils/inputs/inputs";

const AllNews = ({viewNews}) => {
  const [news, setNews] = useState([]);
  const [indexNews, setIndexNews] = useState(0);
  const [isLast, setIsLast] = useState(true);

  useEffect(() => {
    get5news(indexNews).then(e => {
      setNews(e.news);
      setIsLast(e.newsLeft);
    });
  }, [indexNews]);

  return (
    <div className="all-news">
      <MDBIcon
        icon="angle-left"
        className={indexNews !== 0 ? "button-prev" : " button-prev button-prev--disable"}
        onClick={() => indexNews !== 0 && setIndexNews(indexNews - 5)}
      />
      <div className="cards">
        {news &&
          news.map((value, key) => {
            return (
              <div
                key={key}
                onClick={() => viewNews(value)}
                className={("column", key === 0 ? "last" : "other")}
              >
                <Card className="view overlay zoom">
                  <div className="pseudo-bg"></div>

                  <InputsImage
                    className="img-fluid"
                    src={value.picture.name_picture}
                    alt={value.picture.alt_picture}
                  />
                  <div className="card-text">
                    <div className="card-text-title">{value.title_news}</div>
                    <div className="card-text-date">
                      {value.date_news
                        .split("T")[0]
                        .split("-")
                        .reverse()
                        .join("-")}
                    </div>
                  </div>
                  <div className="mask flex-center pseudo-bg-text">
                    <p className="white-text">Lire l'article</p>
                  </div>
                </Card>
              </div>
            );
          })}
      </div>
      {isLast && (
        <MDBIcon
          icon="angle-right"
          className="button-next"
          onClick={() => setIndexNews(indexNews + 5)}
        />
      )}
    </div>
  );
};

export default AllNews;

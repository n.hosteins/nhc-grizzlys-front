import React, {useState} from "react";
import "styles/users.styles/users.styles.news.scss";
import AllNews from "./news.allNews";
import OneNews from "./news.oneNews";

const News = () => {
  const [viewFullNews, setViewFullNews] = useState({});
  const [activeFullView, setActiveFullView] = useState(false);

  const viewNews = news => {
    setViewFullNews(news);
    setActiveFullView(true);
  };

  const backToAllNews = () => {
    setViewFullNews({});
    setActiveFullView(false);
  };

  return (
    <div className="main-news">
      <div className="main-news-container">
        <h2>notre actualité</h2>
        {activeFullView ? (
          <OneNews news={viewFullNews} backToAllNews={() => backToAllNews()} />
        ) : (
          <AllNews viewNews={news => viewNews(news)} />
        )}
      </div>
    </div>
  );
};

export default News;

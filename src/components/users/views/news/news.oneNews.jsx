import React from "react";
import "styles/users.styles/users.styles.news.one.scss";
import {MDBIcon} from "mdbreact";
import {renderHtml} from "utils/inputs/wysiwyg";
import {InputsImage} from "utils/inputs/inputs";

const OneNews = props => {
  return (
    <div className="main-news-single">
      <button className="main-news-single-button" onClick={() => props.backToAllNews()}>
        <MDBIcon icon="angle-left" />
        <span className="button-text">Retour</span>
      </button>
      <div className="main-news-single-body">
        <div className="main-news-single-body-header">
          <h3>{props.news.title_news}</h3>
          <span>
            {props.news.date_news
              .split("T")[0]
              .split("-")
              .reverse()
              .join("-")}
          </span>
        </div>
        <div className="main-news-single-body-content row mx-1 mx-lg-0 ">
          <div className="main-news-single-body-content-picture col-12 col-lg-6 pl-0">
            <InputsImage
              src={props.news.picture.name_picture}
              alt={props.news.picture.alt_picture}
            />
          </div>
          <div className="main-news-single-body-content-text col-12 col-lg-6">
            {renderHtml(props.news.article_news)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default OneNews;

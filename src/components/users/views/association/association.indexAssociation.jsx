import React, {useEffect, useState} from "react";
import {getAllMembers} from "controler/apiService/apiService.club";
import "styles/users.styles/users.styles.club.scss";
import OneMember from "./association.memberAssociation";

const IndexAssociation = () => {
  const [member, setMember] = useState([]);
  useEffect(() => {
    getAllMembers().then(res => {
      setMember(res);
    });
  }, []);

  return (
    <div className="main-club">
      <div className="main-club-container">
        <h2>l'association</h2>
        <div className="main-club-container-content">
          <div className="main-club-container-content-title">Organigramme</div>
          <div className="main-club-container-content-cards">
            {member.length !== 0 &&
              member.map((value, key) => (
                <OneMember
                  key={value.firstname_membre_asso + value.lasttname_membre_asso}
                  member={value}
                  picture={value.picture}
                />
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default IndexAssociation;

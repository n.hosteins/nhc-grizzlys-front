import React from "react";
import {InputsImage} from "utils/inputs/inputs";

const memberAssociation = ({member, picture}) => {
  return (
    <div className="single-card">
      {picture && <InputsImage src={picture.name_picture} alt={picture.alt_picture} />}
      <div className="member-name">
        {member.firstname_membre_asso} {member.lastname_membre_asso.toUpperCase()}
      </div>
      <div className="member-desc">{member.description_membre_asso}</div>
      <div className="member-mail">{member.mail_membre_asso}</div>
      <div className="member-phone">{member.phone_membre_asso}</div>
    </div>
  );
};

export default memberAssociation;

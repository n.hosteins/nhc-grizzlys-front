import React from "react";
import {InputsImage} from "utils/inputs/inputs";
import {renderHtml} from "utils/inputs/wysiwyg";

const OnePartner = ({partner}) => {
  const phoneNumber = partner.phone_partner
    .split("")
    .map((char, index) => {
      if (index % 2 !== 0 && index < 8) {
        return char + ".";
      }
      return char;
    })
    .join("");
  return (
    partner && (
      <div className="single-partner">
        <div className="single-partner-image">
          <InputsImage
            src={partner.picture.name_picture}
            alt={partner.picture.alt_picture}
          />
        </div>
        <div className="single-partner-content">
          <h3>{partner.name_partner}</h3>
          <div className="single-partner-content-desc">
            {renderHtml(partner.description_partner)}
          </div>
          <div className="single-partner-content-street"> {partner.street_partner}</div>
          <div className="single-partner-content-zip&city">{`${partner.zip_partner} ${partner.city_partner}`}</div>
          <a
            href={`mailto:${partner.mail_partner}`}
            className="single-partner-content-mail"
          >
            {partner.mail_partner}
          </a>
          <a
            target="blank"
            href={partner.website_partner}
            className="single-partner-content-website"
          >
            {partner.website_partner}
          </a>
          <p className="single-partner-content-tel"> {phoneNumber}</p>
        </div>
      </div>
    )
  );
};

export default OnePartner;

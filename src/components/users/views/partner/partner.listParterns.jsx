import React, {useEffect, useState} from "react";
import {InputsImage} from "utils/inputs/inputs";

const ListPartners = ({list, action}) => {
  const [allPartners, setAllPartners] = useState([
    {
      name_partner: "",
      description_partner: "",
      city_partner: "",
      zip_partner: 0,
      street_partner: "",
      phone_partner: 0,
      mail_partner: "",
      website_partner: "",
      picture: {
        id_picture: 0,
        name_picture: "",
        alt_picture: "",
      },
    },
  ]);

  useEffect(() => {
    if (list) setAllPartners(list);
  }, [list]);

  return (
    allPartners && (
      <ul className="partners-list">
        {allPartners.map((value, key) => (
          <li
            className={value.isActive ? "partners-list-item active" : "partners-list-item"}
            key={value.name_partner}
            onClick={() => action(value)}
          >
            <button className="partners-list-item-button">
              <div className="partners-list-item-image">
                <InputsImage
                  src={value.picture.name_picture}
                  alt={value.picture.alt_picture}
                  className="partners-list-item-button-image"
                />
              </div>
              <div className="partners-list-item-button-name">{value.name_partner}</div>
            </button>
          </li>
        ))}
      </ul>
    )
  );
};

export default ListPartners;

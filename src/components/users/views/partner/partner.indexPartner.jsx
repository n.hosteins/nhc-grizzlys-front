import React, {useState, useEffect} from "react";
import "styles/users.styles/users.styles.partner.scss";
import ListPartner from "./partner.listParterns";
import OnePartner from "./partner.onePartner";
import {getAllPartners} from "controler/apiService/apiService.Partners";

const IndexPartner = () => {
  const [viewOnePartner, setViewOnePartner] = useState();
  const [listAllPartner, setListAllPartner] = useState([]);

  useEffect(() => {
    getAllPartners().then(res => {
      setListAllPartner(res);
      setViewOnePartner(res[0]);
    });
  }, []);
  
  const handleChange = value => {
    setViewOnePartner(value);
  };
  const allPartners = listAllPartner;
  const listWithActivePartner = viewOnePartner && allPartners.map(partner => {
    if(partner.id_partner === viewOnePartner.id_partner) {
      return {
        ...partner,
        isActive: true,
      }
    }
    return {
      ...partner,
      isActive: false,
    }
  });
    
  return (
    <div className="main-partner">
      <div className="main-partner-container">
        <h2>Nos partenaires</h2>
        <div className="main-partner-container-content">
          <ListPartner list={listWithActivePartner} action={value => handleChange(value)} />
          {viewOnePartner !== undefined && (
            <OnePartner partner={viewOnePartner} picture={viewOnePartner.picture} />
          )}
        </div>
      </div>
    </div>
  );
};

export default IndexPartner;

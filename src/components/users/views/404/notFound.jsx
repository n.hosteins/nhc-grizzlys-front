import React, {useState, useEffect} from "react";
import {Link} from "react-router-dom";
import "styles/404/404.styles.scss";

const NotFound = () => {
  const [state, setstate] = useState(false);

  useEffect(() => {
    let path = window.location.pathname;
    if (path.match(/admin/)) {
      setstate(true);
    } else {
      setstate(false);
    }
  }, []);

  return (
    <div className="notFound">
      <h1>404</h1>
      <h2>Vous n'auriez pas dû arriver ici</h2>
      <Link to={state ? "/admin" : "/"}>Retour sur la patinoire</Link>
    </div>
  );
};

export default NotFound;

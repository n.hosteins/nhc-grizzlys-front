import React from "react";
import Trainings from "./trainings.container.trainings.jsx";

const TrainingsDays = ({day, training}) => {
  return (
    training.length !== 0 && (
      <div className="training-card">
        <h3 className="training-card-title">{day}</h3>
        {training.length !== 0 &&
          training.map((value, key) => {
            return (
              <Trainings
                key={value.start_hour_training + value.cloakroom_training}
                training={value}
              />
            );
          })}
      </div>
    )
  );
};

export default TrainingsDays;

import React from "react";
import "styles/users.styles/users.styles.training.scss";
import {parsingCloakroom} from "utils/utilis.parsingCloakroom";
import {parsingHour} from "utils/utilis.parsingHour";

const Trainings = ({training}) => {
  return (
    <div className="training-card-training">
      <div className="training-card-training-time">
        {parsingHour(training.start_hour_training, "h")} à{" "}
        {parsingHour(training.end_hour_training, "h")}
      </div>
      <div className="training-card-training-teams">
        <div className="training-card-training-teams-string">
          {training.teams.length !== 0 &&
            training.teams.map((value, key) => (
              <div
                key={value.name_team}
                className="training-card-training-teams-string-team"
              >
                {value.name_team}
              </div>
            ))}
        </div>
        <div className="training-card-training-teams-cloakroom">
          {parsingCloakroom(training.cloakroom_training)}
        </div>
      </div>
    </div>
  );
};

export default Trainings;

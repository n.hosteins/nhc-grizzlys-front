import React, {useEffect, useState} from "react";
import {getByPeriodTraining} from "controler/apiService/apiService.Trainings";
import "styles/users.styles/users.styles.training.scss";
import TrainingsDays from "./container/trainings.container.trainingsDays";

const IndexTrainings = () => {
  const [period, setPeriod] = useState("scolaire");
  const [viewDayTraining, setViewDayTraining] = useState([]);

  useEffect(() => {
    getByPeriodTraining(period).then(res => {
      setViewDayTraining(res);
    });
  }, [period]);

  return (
    <div className="main-training">
      <div className="main-training-container">
        <div className="main-training-container-header">
          <h2>Entraînements</h2>
          <div className="main-training-container-header-periods">
            <button
              className={period === "scolaire" ? "period-active" : ""}
              onClick={() => setPeriod("scolaire")}
            >
              Période Scolaire
            </button>
            <button
              className={period === "vacances" ? "period-active" : ""}
              onClick={() => setPeriod("vacances")}
            >
              Vacances
            </button>
          </div>
        </div>
        <div className="main-training-container-content">
          <div className="main-training-container-content-title">
            {period === "vacances" ? "Vacances" : "Période Scolaire"} année 2020-2021
          </div>
          <div className="main-training-container-content-cards">
            {viewDayTraining.length !== 0 &&
              viewDayTraining.days.map((value, key) => (
                <TrainingsDays
                  key={value.day}
                  day={value.day}
                  training={value.trainings}
                />
              ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default IndexTrainings;

import React from "react";
import "styles/users.styles/users.styles.accueil.scss";

const Accueil = () => {
  return (
    <div className="main">
      <div className="main-container">
        <div className="banner d-none d-xl-flex">
          <img src={require("assets/images/player-main.svg")} alt="" />
          <div className="banner-item item-first">
            <div className="banner-item-content">camaraderie</div>
          </div>
          <div className="banner-item item-second">
            <div className="banner-item-content">respect</div>
          </div>
          <div className="banner-item item-third">
            <div className="banner-item-content">persévérance</div>
          </div>
          <div className="banner-item item-fourth">
            <div className="banner-item-content">abnégation</div>
          </div>
        </div>
        <h1 className="main-title">
          <div>niort</div>
          <div>hockey club</div>
        </h1>
        <div className="main-slogan">
          Toujours plus fort, toujours plus loin, toujours plus beau
        </div>
        <button className="main-button">
          <div className="button-text">Nous rejoindre</div>
        </button>
      </div>
    </div>
  );
};

export default Accueil;

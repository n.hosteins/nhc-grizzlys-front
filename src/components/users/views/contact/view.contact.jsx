import {sendMail} from "controler/apiService/apiService.contact";
import React from "react";
import {useState} from "react";
import {ButtonForms, InputsText, InputsTextArea} from "utils/inputs/inputs";

const Contact = () => {
  const [state, setState] = useState({
    name: "",
    mail_address: "",
    phone: "",
    text: "",
  });

  const handleChange = (type, value) => {
    if (type === "text") {
      setState({...state, [type]: value.replace(/(\r\n|\n|\r)/gm, "\\n")});
    } else {
      setState({...state, [type]: value});
    }
  };

  const handleSubmit = () => {
    const forms = new FormData();
    forms.append("params", JSON.stringify(state));
    sendMail(forms);
  };

  return (
    <div style={{display: "flex"}}>
      <div style={{width: "50%", padding: "10%", textAlign: "center"}}>
        <form>
          <InputsText label={"Votre nom"} action={(type, e) => handleChange("name", e)} />
          <InputsText
            label={"Votre adresse email"}
            action={(type, e) => handleChange("mail_address", e)}
          />
          <InputsText
            label={"Votre numéro de téléphone (optionnel)"}
            action={(type, e) => handleChange("phone", e)}
          />
          <InputsTextArea
            label={"Votre message"}
            action={(type, e) => handleChange("text", e)}
          />
          <ButtonForms label={"Envoyer"} action={(type, e) => handleSubmit()} />
        </form>
      </div>
      <div style={{width: "50%", margin: "0 auto"}}></div>
    </div>
  );
};

export default Contact;

import React from "react";

const NavTeam = ({nameTeam, idTeam, handleChangeView}) => {
  return nameTeam && idTeam ? (
    <button onClick={() => handleChangeView(idTeam)}>{nameTeam}</button>
  ) : null;
};

export default NavTeam;

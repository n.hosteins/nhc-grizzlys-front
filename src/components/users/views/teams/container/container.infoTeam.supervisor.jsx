import React from "react";
import {InputsImage} from "utils/inputs/inputs";

const Supervisor = ({name, func, pic}) => {
  return (
    <div style={{display: "inline-flex", margin: "5%"}}>
      {pic && (
        <div style={{width: "60px", borderRadius: "50%"}}>
          <InputsImage
            src={pic.name_picture}
            alt={pic.alt_picture}
            style={{width: "50px", borderRadius: "50%"}}
          />
        </div>
      )}
      <div>
        <p>{name}</p>
        <span>{func}</span>
      </div>
    </div>
  );
};

export default Supervisor;

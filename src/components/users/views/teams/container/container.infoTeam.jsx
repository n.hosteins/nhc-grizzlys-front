import React from "react";
import {InputsImage} from "utils/inputs/inputs";
import {renderHtml} from "utils/inputs/wysiwyg";
import Supervisor from "./container.infoTeam.supervisor";

const InfoTeam = ({info}) => {
  return (
    info && (
      <div className="teamInfo equipe-content-item" style={{width: "100%"}}>
        <h3>{info.name_team}</h3>
        <div
          style={{
            display: "inline-flex",
            width: "100%",
            justifyContent: "space-betewn",
          }}
        >
          <div style={{width: "100%", height: "200px"}}>
            <InputsImage
              src={info.picture.name_picture}
              alt={info.picture.alt_picture}
              style={{width: "100%", height: "100%"}}
            />
          </div>
          <div style={{width: "100%", display: "inline-block"}}>
            <div style={{display: "inline"}}>
              <Supervisor name={info.sup1} func={info.fSup1} pic={info.picSup1} />
            </div>
            <div style={{display: "inline"}}>
              <Supervisor name={info.sup2} func={info.fSup2} pic={info.picSup2} />
            </div>
            <div style={{display: "inline"}}>
              <Supervisor name={info.sup3} func={info.fSup3} pic={info.picSup3} />
            </div>
          </div>
        </div>
        <div style={{height: "35vh"}}>
          <div style={{margin: "5px", width: "100%", height: "100%", overflow: "auto"}}>
            {renderHtml(info.presentation)}
          </div>
        </div>
      </div>
    )
  );
};

export default InfoTeam;

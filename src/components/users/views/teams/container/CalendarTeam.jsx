import Axios from "axios";
import React, {useState, useEffect} from "react";
import {Alert} from "react-bootstrap";
import {url as baseUrl} from "../../../../../controler/apiService/url";
import {parsingHour} from "../../../../../utils/utilis.parsingHour";
import PropTypes from "prop-types";

const Match = ({match}) => {
  const hourText = parsingHour(match.hour_match, "h");
  const epoch = Date.parse(match.date_match);
  const date = new Date(epoch);
  const shortMonth = date.toLocaleString("default", {month: "short"});
  return (
    <div className="match">
      <div className="content">
        <div className="left">
          <div className="date-box">
            <span className="day">{date.getDay()}</span>
            <span className="month">{shortMonth}</span>
          </div>
        </div>
        <div className="right">
          <span className="title">{match.title}</span>
          <span className="subtitle">{hourText + " " + match.location}</span>
        </div>
      </div>
    </div>
  );
};

Match.propTypes = {
  match: PropTypes.object.isRequired,
};

const CalendarTeam = ({teamId}) => {
  const [state, setState] = useState({matchList: []});
  useEffect(() => {
    Axios.get(baseUrl + "/getMatchTeam/" + teamId)
      .then(resp => setState(state => ({...state, matchList: resp.data})))
      .catch(err => setState(state => ({...state, danger: err})));
  }, [teamId]);
  const {matchList, danger} = state;
  return (
    <div className="match-list">
      {(() => {
        if (danger) {
          return <Alert variant="danger">{danger}</Alert>;
        } else if (matchList && matchList.length !== 0) {
          return matchList.map(match => <Match key={match.id_match} match={match} />);
        } else {
          return <p>Pas de matchs</p>;
        }
      })()}
    </div>
  );
};

CalendarTeam.propTypes = {
  teamId: PropTypes.number.isRequired,
};

export default CalendarTeam;

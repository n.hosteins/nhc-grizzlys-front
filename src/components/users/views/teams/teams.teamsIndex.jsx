import React, {useState, useEffect} from "react";
import "styles/users.styles/users.styles.equipe.scss";
import CalendarTeam from "./container/CalendarTeam";
import InfoTeam from "./container/container.infoTeam";
import NavTeam from "./container/container.navTeam";
import NewsTeam from "./container/container.newsTeam";
import {getAllTeams} from "controler/apiService/apiService.Teams";

const IndexTeam = () => {
  const [teams, setTeams] = useState();
  const [objByOneTeam, setObjByOneTeam] = useState();

  useEffect(() => {
    getAllTeams().then(res => {
      setTeams(res);
      setObjByOneTeam(res[0]);
    });
  }, []);

  const findObjTeam = id => {
    const found = teams.find(element => element.id_team_teams === id);
    setObjByOneTeam(found);
  };

  const splitingByInfo = () => {
    return {
      name_team: objByOneTeam.name_team,
      picture: objByOneTeam.picture,
      presentation: objByOneTeam.presentation_team,
      sup1: objByOneTeam.supervisor_1,
      sup2: objByOneTeam.supervisor_2,
      sup3: objByOneTeam.supervisor_3,
      fSup1: objByOneTeam.function_supervisor1,
      fSup2: objByOneTeam.function_supervisor2,
      fSup3: objByOneTeam.function_supervisor3,
      picSup1: objByOneTeam.pic_supervisor1,
      picSup2: objByOneTeam.pic_supervisor2,
      picSup3: objByOneTeam.pic_supervisor3,
    };
  };

  return (
    <div className="main-equipe">
      <div className="main-equipe-container">
        <div className="main-equipe-container-header">
          <h2>Nos équipes</h2>
          <ul className="main-equipe-container-header-nav">
            {teams &&
              teams.map((value, key) => {
                return (
                  <li key={value.name_team}>
                    <NavTeam
                      nameTeam={value.name_team}
                      idTeam={value.id_team_teams}
                      handleChangeView={id => findObjTeam(id)}
                    />
                  </li>
                );
              })}
          </ul>
        </div>

        {objByOneTeam && (
          <div className="main-equipe-container-content">
            <InfoTeam info={splitingByInfo()} />
            <div
              className="nextGames equipe-content-item"
              style={{backgroundColor: "white", width: "100%", height: "100%"}}
            >
              <h3>Prochains matchs</h3>
              <CalendarTeam teamId={objByOneTeam.id_team_teams} />
            </div>
            <div
              className="teamNews equipe-content-item"
              style={{backgroundColor: "white", width: "100%", height: "100%"}}
            >
              <h3>Actualités</h3>
              <NewsTeam news={objByOneTeam} />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default IndexTeam;

import React from "react";
import Accueil from "./views/accueil/view.accueil";
import Association from "./views/association/association.indexAssociation";
import Contact from "./views/contact/view.contact";
import IndexTraining from "./views/trainings/trainings.indexTraining";
import Equipe from "./views/teams/teams.teamsIndex";
import News from "./views/news/news.newsIndex";
import Partner from "./views/partner/partner.indexPartner";

export const ListComponents = [
  {
    id: 0,
    name: "Accueil",
    Component: <Accueil />,
  },
  {
    id: 1,
    name: "Équipes",
    Component: <Equipe />,
  },
  {
    id: 2,
    name: "L'association",
    Component: <Association />,
  },
  {
    id: 3,
    name: "Actualités",
    Component: <News />,
  },
  {
    id: 4,
    name: "Entraînements",
    Component: <IndexTraining />,
  },
  {
    id: 5,
    name: "Partenaires",
    Component: <Partner />,
  },
  {
    id: 6,
    name: "Contact",
    Component: <Contact />,
  },
];

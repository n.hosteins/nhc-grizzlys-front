import React, {useState} from "react";
import "styles/users.styles/users.styles.scss";
import NavBar from "./view.navBar";
import {Container} from "react-bootstrap";
import {ListComponents} from "./user.list.components";
import {MDBBtn} from "mdbreact";

const Users = () => {
  const [curentPage, setCurentPage] = useState(0);

  const refs = ListComponents.reduce((acc, value) => {
    acc[value.id] = React.createRef();
    return acc;
  }, {});

  const handleClick = id => {
    if (id > 0 && id < ListComponents.length) {
      refs[id].current.scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
      setCurentPage(id);
    } else if (id === 0) {
      const yOffset = -100;
      const element = document.getElementById(id);
      const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
      window.scrollTo({top: y, behavior: "smooth"});
      setCurentPage(id);
    }
  };

  return (
    <Container id="acc" fluid className="full-screen">
      <NavBar handleClick={id => handleClick(id)} />

      {ListComponents.map(item => {
        return (
          <div className="ToScroll" key={item.id} id={item.id} ref={refs[item.id]}>
            {item.Component}
          </div>
        );
      })}

      <div className="btnTop">
        <MDBBtn onClick={() => handleClick(curentPage - 1)} gradient="blue">
          -
        </MDBBtn>
        <MDBBtn onClick={() => handleClick(0)} gradient="blue">
          Accueil
        </MDBBtn>
        <MDBBtn onClick={() => handleClick(curentPage + 1)} gradient="blue">
          +
        </MDBBtn>
      </div>
    </Container>
  );
};

export default Users;

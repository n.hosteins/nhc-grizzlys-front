export default [
  {
    name_partner: "Berger Location",
    description_partner:
      "Location de voitures, de véhicules utilitaires et de poids lourds, pour le professionel et le particulier à Niort.",
    city_partner: "Les Belleville",
    zip_partner: 73440,
    street_partner: "Station Les Ménuires",
    phone_partner: "0479006144",
    mail_partner: "contact@matt-sports-montagne.com",
    website_partner: "",
    picture: {
      id_picture: 43,
      name_picture: "",
      alt_picture: "",
    },
  },
  {
    name_partner: "Crédit mutuelle",
    description_partner:
      "Location de voitures, de véhicules utilitaires et de poids lourds, pour le professionel et le particulier à Niort.",
    city_partner: "Les Belleville",
    zip_partner: 73440,
    street_partner: "Station Les Ménuires",
    phone_partner: "0479006144",
    mail_partner: "contact@matt-sports-montagne.com",
    website_partner: "",
    picture: {
      id_picture: 43,
      name_picture: "",
      alt_picture: "",
    },
  },
  {
    name_partner: "ESLAN Polyclinique Ikermann",
    description_partner:
      "Location de voitures, de véhicules utilitaires et de poids lourds, pour le professionel et le particulier à Niort.",
    city_partner: "Les Belleville",
    zip_partner: 73440,
    street_partner: "Station Les Ménuires",
    phone_partner: "0479006144",
    mail_partner: "contact@matt-sports-montagne.com",
    website_partner: "",
    picture: {
      id_picture: 43,
      name_picture: "",
      alt_picture: "",
    },
  },
  {
    name_partner: "Matt Sports Montagne",
    description_partner:
      "Location de voitures, de véhicules utilitaires et de poids lourds, pour le professionel et le particulier à Niort.",
    city_partner: "Les Belleville",
    zip_partner: 73440,
    street_partner: "Station Les Ménuires",
    phone_partner: "0479006144",
    mail_partner: "contact@matt-sports-montagne.com",
    website_partner: "",
    picture: {
      id_picture: 43,
      name_picture: "",
      alt_picture: "",
    },
  },
  {
    name_partner: "Newsports",
    description_partner:
      "Location de voitures, de véhicules utilitaires et de poids lourds, pour le professionel et le particulier à Niort.",
    city_partner: "Les Belleville",
    zip_partner: 73440,
    street_partner: "Station Les Ménuires",
    phone_partner: "0479006144",
    mail_partner: "contact@matt-sports-montagne.com",
    website_partner: "",
    picture: {
      id_picture: 43,
      name_picture: "",
      alt_picture: "",
    },
  },
];

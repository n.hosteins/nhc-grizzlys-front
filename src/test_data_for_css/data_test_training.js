export default {
  days: [
    {
      day: "lundi",
      trainings: [
        {
          start_hour_training: "19h00",
          end_hour_training: "20h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u11",
            },
            {
              name_team: "u13",
            },
          ],
        },
        {
          start_hour_training: "20h00",
          end_hour_training: "21h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u15",
            },
            {
              name_team: "u17",
            },
            {
              name_team: "u20",
            },
          ],
        },
        {
          start_hour_training: "19h00",
          end_hour_training: "20h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u11",
            },
            {
              name_team: "u13",
            },
          ],
        },
      ],
    },
    {
      day: "mardi",
      trainings: [
        {
          start_hour_training: "19h00",
          end_hour_training: "20h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u11",
            },
            {
              name_team: "u13",
            },
          ],
        },
        {
          start_hour_training: "20h00",
          end_hour_training: "21h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u15",
            },
            {
              name_team: "u17",
            },
            {
              name_team: "u20",
            },
          ],
        },
      ],
    },
    {day: "mercredi", trainings: []},
    {day: "jeudi", trainings: []},
    {
      day: "vendredi",
      trainings: [
        {
          start_hour_training: "19h00",
          end_hour_training: "20h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u11",
            },
            {
              name_team: "u13",
            },
          ],
        },
        {
          start_hour_training: "20h00",
          end_hour_training: "21h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u15",
            },
            {
              name_team: "u17",
            },
            {
              name_team: "u20",
            },
          ],
        },
      ],
    },
    {
      day: "samedi",
      trainings: [
        {
          start_hour_training: "19h00",
          end_hour_training: "20h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u11",
            },
            {
              name_team: "u13",
            },
          ],
        },
        {
          start_hour_training: "20h00",
          end_hour_training: "21h00",
          cloakroom_training: "A",
          teams: [
            {
              name_team: "u15",
            },
            {
              name_team: "u17",
            },
            {
              name_team: "u20",
            },
          ],
        },
      ],
    },
    {day: "dimanche", trainings: []},
  ],
};

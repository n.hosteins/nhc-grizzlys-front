import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Admin from "./components/admin/admin.index";
import Users from "./components/users/users.index";
import Login from "./components/auth/auth.login";
import PrivateRoute from "./controler/controler.privateRoute";
import NotFound from "components/users/views/404/notFound";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Users} />
        <Route exact path="/connexion" component={Login} />
        <PrivateRoute exact path="/admin" component={Admin} />
        <PrivateRoute exact path="/admin/*" component={Admin} />
        <Route path="" component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;

export const ControllerStateUpdateTeam = (data, returnData) => {
  const team = {
    id_team_teams: data.id_team_teams,
    name_team: data.name_team,
    presentation_team: data.presentation_team,
    picture: {
      id_picture: data.picture.id_picture,
      name_picture: data.picture.name_picture,
      alt_picture: data.picture.alt_picture,
    },
  };

  const supervisor = {
    supervisor_1: data.supervisor_1,
    function_supervisor1: data.function_supervisor1,
    supervisor_2: data.supervisor_2,
    function_supervisor2: data.function_supervisor2,
    supervisor_3: data.supervisor_3,
    function_supervisor3: data.function_supervisor3,
    pic_supervisor1: data.pic_supervisor1,
    pic_supervisor2: data.pic_supervisor2,
    pic_supervisor3: data.pic_supervisor3,
  };
  returnData(team, supervisor);
};

const DefaultTeam = {
  id_team_teams: "",
  name_team: "",
  presentation_team: "",

  picture: {
    id_picture: "",
    name_picture: "",
    alt_picture: "",
  },
};

const DefaultSup = {
  supervisor_1: "",
  function_supervisor1: "",
  supervisor_2: "",
  function_supervisor2: "",
  supervisor_3: "",
  function_supervisor3: "",

  pic_supervisor1: {
    id_picture: "",
    name_picture: "",
    alt_picture: "",
  },
  pic_supervisor2: {
    id_picture: "",
    name_picture: "",
    alt_picture: "",
  },
  pic_supervisor3: {
    id_picture: "",
    name_picture: "",
    alt_picture: "",
  },
};

export {DefaultTeam, DefaultSup};

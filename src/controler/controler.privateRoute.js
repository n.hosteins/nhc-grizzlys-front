import React, {useEffect} from "react";
import {Redirect, Route} from "react-router-dom";

const PrivateRoute = ({component: Component, ...rest}) => {
  useEffect(() => {}, []);
  return (
    <Route
      {...rest}
      render={props => (true ? <Component {...props} /> : <Redirect to="/connexion" />)}
    />
  );
};

export default PrivateRoute;

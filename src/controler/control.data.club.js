export const defaultStateClub = {
  firstname_membre_asso: "",
  lastname_membre_asso: "",
  phone_membre_asso: 0,
  mail_membre_asso: "",
  description_membre_asso: "",
};

export const ControllerStateUpdateClub = (data, returnData) => {
  const member = {
    firstname_membre_asso: data.firstname_membre_asso,
    lastname_membre_asso: data.lastname_membre_asso,
    phone_membre_asso: data.phone_membre_asso,
    mail_membre_asso: data.mail_membre_asso,
    description_membre_asso: data.description_membre_asso,
  };
  const picture = {
    id_picture: data.picture.id_picture,
    name_picture: data.picture.name_picture,
    alt_picture: data.picture.alt_picture,
  };
  returnData(member, picture);
};

import {getCurrentDate} from "utils/utilis.getCurentDate";

export const ControllerStateUpdateNews = (data, returnData) => {
  const news = {
    article_news: data.article_news,
    date_news: data.date_news,
    title_news: data.title_news,
  };
  const picture = {
    id_picture: data.picture.id_picture,
    name_picture: data.picture.name_picture,
    alt_picture: data.picture.alt_picture,
  };

  const teams = data.teams.map(x => {
    return x.id_team_teams;
  });
  returnData(news, picture, teams);
};

export const defaultStateNews = {
  article_news: "",
  date_news: getCurrentDate(),
  title_news: "",
};

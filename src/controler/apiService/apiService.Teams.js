import Axios from "axios";
import {url} from "./url";

export const getAllTeams = async () => {
  const res = await Axios.get(url + "/getAllTeams");
  return res.data;
};

export const getOneTeam = async id => {
  const res = await Axios.get(url + "/getTeam/" + id);
  return res.data;
};

export const postTeam = async x => {
  try {
    const res = await Axios.post(url + "/addTeam", x);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const putTeam = async (id, x) => {
  try {
    const res = await Axios.put(url + "/updateTeam/" + id, x);
    return res.data;
  } catch (err) {
    return err;
  }
};
export const deleteTeam = async id => {
  try {
    const res = await Axios.delete(url + "/deleteTeam/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
};

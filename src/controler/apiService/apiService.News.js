import Axios from "axios";
import {url} from "./url";

export async function get5news(x) {
  try {
    const res = await Axios.get(url + "/getNews/" + x);
    return res.data;
  } catch (err) {
    return err;
  }
}

export async function getOneNews(id) {
  try {
    const res = await Axios.get(url + "/getNewsById/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
}
export const getNewsByTitle = async value => {
  try {
    const res = await Axios.get(url + "/getNewsByTitle/" + value);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const deleteNews = async id => {
  try {
    const res = await Axios.delete(url + "/deleteNews/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const updateNews = async (id, data) => {
  try {
    const res = await Axios.put(url + "/updateNews/" + id, data);
    return res.data;
  } catch (err) {
    return err;
  }
};

export async function postNews(x) {
  try {
    const res = await Axios.post(url + "/addNews", x);
    return res.data;
  } catch (err) {
    return err;
  }
}

export async function get20news(x) {
  try {
    const res = await Axios.get(url + "/get20News/" + x);
    return res.data;
  } catch (err) {
    return err;
  }
}

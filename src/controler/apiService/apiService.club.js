import Axios from "axios";
import {url} from "./url";

export const getAllMembers = async () => {
  try {
    const res = await Axios.get(url + "/getAllMembreAsso");
    return res.data;
  } catch (err) {
    return err;
  }
};

export const getByIdMember = async id => {
  try {
    const res = await Axios.get(url + "/getOneMembreAsso/" + id);
    return res;
  } catch (err) {
    return err;
  }
};

export const postMember = async data => {
  try {
    const res = await Axios.post(url + "/addMembreAsso", data);
    return res;
  } catch (err) {
    return err;
  }
};

export const putMember = async (id, data) => {
  try {
    const res = await Axios.put(url + "/updateMembreAsso/" + id, data);
    return res;
  } catch (err) {
    return err;
  }
};

export const deleteMembersClub = id => {
  return Axios.delete(url + "/deleteMembreAsso/" + id)
    .then(res => {
      return res;
    })
    .catch(err => err);
};

import Axios from "axios";
import {url} from "./url";

export const getContact = async id => {
  try {
    const res = await Axios.get(url + "/getContact/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const sendMail = async data => {
  try {
    const res = await Axios.post(url + "/sendMail", data);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const updateContact = async data => {
  try {
    const res = await Axios.put(url + "/updateContact", data);
    return res.data;
  } catch (err) {
    return err;
  }
};

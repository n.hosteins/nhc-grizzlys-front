import Axios from "axios";
import {url} from "./url";

export const get20Match = async x => {
  try {
    const res = await Axios.get(url + "/get20Match/" + x);

    return res.data;
  } catch (err) {
    return err;
  }
};

export const addMatch = async data => {
  try {
    const res = await Axios.post(url + "/addMatch", data);
    return res.data;
  } catch (err) {
    return err;
  }
};
export const deleteMatch = async id => {
  try {
    const res = await Axios.delete(url + "/deleteMatch/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const updateMatch = async (id, data) => {
  try {
    const res = await Axios.put(url + "/updateMatch/" + id, data);
    return res.data;
  } catch (err) {
    return err;
  }
};

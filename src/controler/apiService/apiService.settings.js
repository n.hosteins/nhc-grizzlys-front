import Axios from "axios";
import {url} from "./url";

export const postSetting = async data => {
  try {
    const res = await Axios.get(url + "/postSettings/", data);
    return res.data;
  } catch (err) {
    return err;
  }
};

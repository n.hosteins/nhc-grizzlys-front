import Axios from "axios";
import {url} from "./url";

export const getAllPartners = async () => {
  try {
    const res = await Axios.get(url + "/getAllPartners");
    return res.data;
  } catch (err) {
    return err;
  }
};

export const deletePartner = async id => {
  try {
    const res = await Axios.delete(url + "/deletePartner/" + id);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const updatePartner = async (id, data) => {
  try {
    const res = await Axios.put(url + "/updatePartner/" + id, data);
    return res.data;
  } catch (err) {
    return err;
  }
};

export const addPartner = async data => {
  try {
    const res = await Axios.post(url + "/addPartner", data);
    return res.data;
  } catch (err) {
    return err;
  }
};

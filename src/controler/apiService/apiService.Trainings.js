import Axios from "axios";
import {url} from "./url";

export const getAllTrainings = async () => {
  const res = await Axios.get(url + "/getAllTrainings");
  return res.data;
};
export const getAllTrainingsByAdmin = async () => {
  const res = await Axios.get(url + "/getAllTrainings");
  return res.data;
};
export const getByPeriodTraining = async period => {
  const res = await Axios.get(url + "/getTrainingPeriod/" + period);
  return res.data;
};

export const postTraining = async data => {
  try {
    const res = await Axios.post(url + "/addTraining", data);
    return res;
  } catch (err) {
    return err;
  }
};

export const putTraining = async (id, data) => {
  try {
    const res = await Axios.put(url + "/updateTraining/" + id, data);
    return res;
  } catch (err) {
    return err;
  }
};

export const deleteTraining = async id => {
  try {
    const res = await Axios.delete(url + "/deleteTraining/" + id);
    return res;
  } catch (err) {
    return err;
  }
};

export const getExceptionPeriod = () => {
  return "Vacances";
};

import Axios from "axios";
import Crypt from "../../utils/utilis.encrypt";
import {url} from "./url";

export async function postLogin(data) {
  let password = "";
  Crypt.CryptContent(JSON.stringify(data.password), res => (password = res));
  const res_1 = await Axios.post(url + "/login", {
    username: data.username,
    password: password,
  });
  return res_1.data.token;
}

export const ControllerStateUpdatePartner = (data, returnData) => {
  const partner = {
    city_partner: data.city_partner,
    description_partner: data.description_partner,
    mail_partner: data.mail_partner,
    name_partner: data.name_partner,
    phone_partner: data.phone_partner,
    street_partner: data.street_partner,
    website_partner: data.website_partner,
    zip_partner: data.zip_partner,
  };
  const picture = {
    id_picture: data.picture.id_picture,
    name_picture: data.picture.name_picture,
    alt_picture: data.picture.alt_picture,
  };
  returnData(partner, picture);
};

export const defaultStatePartner = {
  city_partner: "",
  description_partner: "",
  mail_partner: "",
  name_partner: "",
  phone_partner: "",
  street_partner: "",
  website_partner: "",
  zip_partner: "",
};

export const parsingCloakroom = cloakroom => {
  let cloakroomToTab = cloakroom.split("_");
  let cloakroomToString = "";
  if (cloakroom.length > 1) {
    cloakroomToString = "Vestiaires " + cloakroomToTab.join(" et ");
  } else {
    cloakroomToString = "Vestiaire " + cloakroomToTab;
  }
  return cloakroomToString;
};

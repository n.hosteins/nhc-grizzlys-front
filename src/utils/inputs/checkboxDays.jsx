import React from "react";
import {Col, Form, Row} from "react-bootstrap";

const CheckBoxDays = ({day_training, action}) => {
  const days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];

  return (
    <Form.Group as={Row}>
      <Form.Label as="legend" column sm={2}>
        Jours :
      </Form.Label>
      <Col sm={10}>
        {days.map((value, key) => {
          return (
            <Form.Check
              key={key}
              type="radio"
              label={value}
              name="formHorizontalRadios"
              id={"formHorizontalRadios" + key}
              checked={day_training.toLowerCase() === value.toLowerCase() ? true : false}
              onChange={() => action(value)}
            />
          );
        })}
      </Col>
    </Form.Group>
  );
};

export default CheckBoxDays;

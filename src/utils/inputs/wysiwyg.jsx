import React, {useEffect, useState} from "react";
import {EditorState, convertToRaw, ContentState} from "draft-js";
import {Editor} from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

const MAX_LENGTH = 4096;
/** FIXME :
 * convert hooks to class because warning console of the setState
 *
 */
const Wysiwyg = ({action, label, type, defaultValue}) => {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  useEffect(() => {
    if (defaultValue) {
      const contentBlock = htmlToDraft(defaultValue);
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(
          contentBlock.contentBlocks
        );
        const editor = EditorState.createWithContent(contentState);
        setEditorState(editor);
      }
    }
  }, [defaultValue]);

  const getLengthOfSelectedText = () => {
    const currentSelection = editorState.getSelection();
    const isCollapsed = currentSelection.isCollapsed();
    let length = 0;
    if (!isCollapsed) {
      const currentContent = editorState.getCurrentContent();
      const startKey = currentSelection.getStartKey();
      const endKey = currentSelection.getEndKey();
      const startBlock = currentContent.getBlockForKey(startKey);
      const isStartAndEndBlockAreTheSame = startKey === endKey;
      const startBlockTextLength = startBlock.getLength();
      const startSelectedTextLength =
        startBlockTextLength - currentSelection.getStartOffset();
      const endSelectedTextLength = currentSelection.getEndOffset();
      const keyAfterEnd = currentContent.getKeyAfter(endKey);
      if (isStartAndEndBlockAreTheSame) {
        length += currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        let currentKey = startKey;
        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }
          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }
    return length;
  };

  const handleBeforeInput = () => {
    const currentContent = editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = getLengthOfSelectedText();
    if (currentContentLength - selectedTextLength > MAX_LENGTH - 1) {
      return true;
    }
  };

  const handlePastedText = pastedText => {
    const currentContent = editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText("").length;
    const selectedTextLength = getLengthOfSelectedText();
    if (currentContentLength + pastedText.length - selectedTextLength > MAX_LENGTH) {
      return true;
    }
  };

  const onEditorStateChange = editorState => {
    setEditorState(editorState);
    action(draftToHtml(convertToRaw(editorState.getCurrentContent())));
  };

  return (
    <div>
      <p>{label}</p>
      <div style={{border: "1px solid black"}}>
        <Editor
          editorState={editorState}
          wrapperClassName="wrapper"
          editorClassName="editor"
          onEditorStateChange={editorState => onEditorStateChange(editorState)}
          handleBeforeInput={() => handleBeforeInput()}
          handlePastedText={pastedText => handlePastedText(pastedText)}
          toolbar={toolbar}
        />
      </div>
    </div>
  );
};
export default Wysiwyg;

export const toolbar = {
  options: ["inline", "blockType", "fontSize", "list", "textAlign", "link", "history"],
  inline: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["bold", "italic", "underline"],
    bold: {className: undefined},
    italic: {className: undefined},
    underline: {className: undefined},
    strikethrough: {className: undefined},
    monospace: {className: undefined},
  },
  blockType: {
    inDropdown: true,
    options: ["Normal", "H1", "H2", "H3", "H4", "H5", "H6", "Blockquote", "Code"],
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
  },
  fontSize: {
    options: [8, 9, 10, 11, 12, 14, 16, 18, 24, 30, 36, 48, 60, 72, 96],
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
  },
  list: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["unordered", "ordered"],
    unordered: {className: undefined},
    ordered: {className: undefined},
  },
  textAlign: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["left", "center", "right", "justify"],
    left: {className: undefined},
    center: {className: undefined},
    right: {className: undefined},
    justify: {className: undefined},
  },
  link: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    popupClassName: undefined,
    dropdownClassName: undefined,
    showOpenOptionOnHover: true,
    defaultTargetOption: "_self",
    options: ["link", "unlink"],
    link: {className: undefined},
    unlink: {className: undefined},
    linkCallback: undefined,
  },
  history: {
    inDropdown: false,
    className: undefined,
    component: undefined,
    dropdownClassName: undefined,
    options: ["undo", "redo"],
    undo: {className: undefined},
    redo: {className: undefined},
  },
};

export const renderHtml = text => {
  return <div dangerouslySetInnerHTML={{__html: text}} />;
};

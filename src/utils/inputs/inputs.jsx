import {url} from "controler/apiService/url";
import React from "react";
import {Form, Button} from "react-bootstrap";
import "react-times/css/material/default.css";
import {getCurrentHour} from "utils/utilis.getCurentDate";

export const InputsText = ({label, type, defaultValue, action}) => {
  return (
    <Form.Group controlId={"formBasicText" + label}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        required
        type="text"
        value={defaultValue}
        onChange={e => action(type, e.target.value)}
      />
    </Form.Group>
  );
};

export const InputsFile = ({label, action}) => {
  return (
    <Form.Group controlId={"formBasicFile" + label}>
      <Form.Label>{label}</Form.Label>
      <Form.File onChange={e => action(e.target.files)} accept="image/jpeg" />
    </Form.Group>
  );
};
export const CheckBox = ({label, value, id, action, isChecked}) => {
  return (
    <Form.Group>
      <Form.Label>{label}</Form.Label>
      <Form.Check
        id={id}
        type="checkbox"
        checked={isChecked}
        onChange={() => action(value)}
      />
    </Form.Group>
  );
};

export const RadioBox = ({label, value, id, action, name, isChecked}) => {
  return (
    <Form.Group controlId="formBasicCheckbox">
      <Form.Check
        type="radio"
        name={name}
        label={label}
        id={id}
        checked={isChecked}
        onChange={() => action(value)}
      />
    </Form.Group>
  );
};

export const ButtonForms = ({label, action}) => {
  return (
    <Button variant="primary" onClick={e => action(e)}>
      {label}
    </Button>
  );
};

export const InputsTime = ({action, label, defaultValue, type}) => {
  return (
    <Form.Group controlId="fromBasicHour">
      <Form.Label>{label}</Form.Label>
      <input
        required
        type="time"
        value={defaultValue ? defaultValue : getCurrentHour()}
        onChange={e => action(type, e)}
      />
    </Form.Group>
  );
};

export const InputsImage = ({src, alt, style}) => {
  return src && <img src={url + "/files/" + src} alt={alt} style={style} />;
};

export const InputsTextArea = ({label, type, defaultValue, action}) => {
  return (
    <Form.Group controlId="formBasicText">
      <Form.Label>{label}</Form.Label>
      <textarea
        rows="5"
        cols="33"
        value={defaultValue}
        onChange={e => action(type, e.target.value)}
      />
    </Form.Group>
  );
};

import React, {useEffect, useState} from "react";
import {getAllTeams} from "controler/apiService/apiService.Teams";
import {CheckBox} from "./inputs";

const CheckboxEquipes = ({returnValue, teamSelected}) => {
  const [teams, setTeams] = useState([]);
  const [returnTeams, setReturnTeams] = useState([]);

  useEffect(() => {
    getAllTeams().then(res => {
      setTeams(res);
    });
    if (teamSelected.length !== 0) {
      setReturnTeams(teamSelected);
    }
  }, [teamSelected]);

  const clickTeam = id => {
    let temp = [...returnTeams];
    if (temp.includes(id)) {
      temp.splice(temp.indexOf(id), 1);
    } else {
      temp.push(id);
    }
    returnValue(temp);
    setReturnTeams(temp);
  };

  return teams.map((value, key) => {
    return (
      <CheckBox
        label={value.name_team}
        value={value.id_team_teams}
        isChecked={returnTeams.includes(value.id_team_teams)}
        action={e => clickTeam(e)}
        key={key}
      />
    );
  });
};
export default CheckboxEquipes;

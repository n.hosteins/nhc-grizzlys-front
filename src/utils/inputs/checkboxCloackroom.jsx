import React, {useEffect, useState} from "react";
import {Form} from "react-bootstrap";
import {CheckBox} from "./inputs";

const CheckboxCloackroom = ({returnValue, cloackroomSelected}) => {
  const [cloackroom] = useState(["A", "B"]);
  const [returnCloackrooms, setReturnCloackrooms] = useState([]);

  useEffect(() => {
    if (cloackroomSelected) {
      setReturnCloackrooms(cloackroomSelected);
    }
  }, [cloackroomSelected]);

  const clickCloackroom = id => {
    let temp = [...returnCloackrooms];
    if (temp.includes(id)) {
      temp.splice(temp.indexOf(id), 1);
    } else {
      temp.push(id);
    }

    returnValue(temp);
    setReturnCloackrooms(temp);
  };

  return (
    <Form.Group controlId="formBasicCheckboxCloackroom">
      {cloackroom.map((value, key) => {
        return (
          <CheckBox
            label={value}
            value={value}
            id={key}
            isChecked={returnCloackrooms.includes(value)}
            action={e => clickCloackroom(e)}
            key={key}
          />
        );
      })}
    </Form.Group>
  );
};
export default CheckboxCloackroom;

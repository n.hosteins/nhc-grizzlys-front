export function getCurrentDate() {
  let newDate = new Date();
  //let date = newDate.getDate();
  //let month = newDate.getMonth() + 1;
  //let year = newDate.getFullYear();
  return newDate;
}

export function getCurrentHour() {
  let newDate = new Date();
  let h = newDate.getHours();
  let m = newDate.getMinutes();
  return JSON.stringify(h + ":" + m);
}

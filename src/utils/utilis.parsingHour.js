export const parsingHour = (hour, methode = ":") => {
  let hourToArray = hour.toString().split("");
  if (hourToArray.length < 4) {
    do {
      hourToArray.unshift("0");
    } while (hourToArray.length < 4);
  }
  let h = hourToArray.splice(0, 2).join("");
  let m = hourToArray.join("");
  return "" + h + methode + m;
};
